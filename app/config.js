import config from 'config';
import dotenv from 'dotenv';
import envs from './constants/envs';
import path from 'path';
import env from './utils/env';
import evalStringMultiply from './utils/evalStringMultiply';
dotenv.config();

if (!envs[env]) {
    throw Error(`unknown env '${env}'`);
}

const DIST_DIR = path.join(__dirname, '../client/dist');
const PORT = process.env.PORT || config.get('port');
const MONGO_URI = process.env.MONGO_URI || config.get('mongo.uri');
const MSSQL_OPTIONS = config.get('mssql');
const JWT_SECRET = config.get('jwt.secret');
const JENGA_URL = config.get('jenga.url');
const OLYMP_URL = config.get('olymp.url');
const SEARCH_PARAMS = process.env.SEARCH_PARAMS || config.get('search_params');

const MAX_AGE = process.env.MAX_AGE || config.get('max_age');
const SERVE_OPTIONS = {
    maxage: evalStringMultiply(MAX_AGE)
};

if (!JWT_SECRET) {
    throw Error('You must jwt secret string!');
}

export {
    PORT,
    MONGO_URI,
    MSSQL_OPTIONS,
    JWT_SECRET,
    DIST_DIR,
    SERVE_OPTIONS,
    JENGA_URL,
    OLYMP_URL,
    SEARCH_PARAMS
};
