import Router from 'koa-router';
import remoteController from './controllers/remote-controller';
import checkUser from '../../handlers/checkUser';


const router = new Router({ prefix: '/api/remote' });

router
    .post('/bookings', checkUser(), remoteController.getBookings)
    .get('/bookings/:id', checkUser(), remoteController.getBookingDataById)
    .get('/search-geo', remoteController.searchGeo)
    .post('/search-products', remoteController.searchProducts)
    .post('/collected-products', remoteController.collectedProducts)
    .get('/get-price', remoteController.getPrice);

export default router.routes();
