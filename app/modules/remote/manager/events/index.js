import moment from 'moment';
import fs from 'fs';
import path from 'path';
import isString from 'lodash/isString';
import last from 'lodash/last';
import { loadSqlQueries, prepareFormats } from '../utils';

const writeToJson = (filename, data) => {
    let strJsonData = JSON.stringify(data);
    fs.writeFile(path.resolve(
        __dirname, 'jsonData', `${filename}.json`
    ), strJsonData, 'utf8', () => {});
};

function collectFields(recordset) {

    let fieldValues = [];

    for (let row of recordset) {

        for (let key in row) {

           // if (!~fieldValues.indexOf(key) && row[key] !== null && row[key] !== 0 && row[key] !== '0' && row[key]) {
            if (!~fieldValues.indexOf(key) && row[key] !== null) {

                console.log(row[key]);

                if (isString(row[key])) {
                  if (row[key].trim().length) {
                    fieldValues.push(key);
                  }
                } else {
                    fieldValues.push(key);
                }

            }
        }
    }
    debugger
    fieldValues.join(',\n')
};

const register = async ( { sql, getConnection } ) => {

    const sqlQueries = await loadSqlQueries( "events" );

    const getTemp = async () => {

        const cnx = await getConnection();

        debugger

        const { recordset } = await cnx.request()
            .query(sqlQueries.tmp);

        debugger

        collectFields(recordset);
    };

    const getBookings = async ({
         startDate = new Date(),
         endDate = new Date(),
         isModify = false
    }) => {

        const cnx = await getConnection();
        const request = await cnx.request();

        for (let curr of  [{
            column: 'BOOKINGSTARDATE',
            value: startDate
        }, {
            column: 'BOOKINGENDDATE',
            value: endDate
        }]) {
           request.input(curr.column, sql.NVarChar,
               moment(curr.value).format('YYYY/MM/DD')
           );
        }

        const { recordset } = await request
            .query(sqlQueries.getBookings);

        return prepareFormats(recordset, isModify);
    };

    const getDataByParam = async (isModify, fieldName, id, method) => {

        const cnx = await getConnection();
        const { recordset }  = await cnx.request()
            .input(fieldName, sql.INT, id)
            .query(sqlQueries[method]);

        return prepareFormats(recordset, isModify);
    };

    const getDataByParams = async (isModify, fieldName, values, method) => {

        const cnx = await getConnection();
        const request = await cnx.request();

        const parameterNames = [];
        const parameterName = last(fieldName.split('.'));
        for (let i = 0; i < values.length; i++) {
          let currentParameter = parameterName + i;
          request.input(currentParameter, sql.INT, values[i]);
          parameterNames.push(`@${currentParameter}`);
        }

        const partQueryStr = `${fieldName} IN (${parameterNames.join(',')})`;
        const queryStr = `${sqlQueries[method]}${partQueryStr}`;
        const { recordset } = await request.query(queryStr);

        return prepareFormats(recordset, isModify);
    };

    return {
        getDataByParams,
        getDataByParam,
        getBookings,
        getTemp
    };
};

export {
  register
};
