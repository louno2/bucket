SELECT
  CR.CHILDRATEID      AS 'prices.child.id',
  CR.CHILDRATEAMOUNT  AS 'prices.child.amount',
  CR.CHILDRATEFROMAGE AS 'prices.child.from',
  CR.CHILDRATETOAGE   AS 'prices.child.to',
  CR.PRICEID AS 'prices.priceId'
FROM
  CHILD_RATE AS CR
WHERE
  CR.PRICEID = :priceId
