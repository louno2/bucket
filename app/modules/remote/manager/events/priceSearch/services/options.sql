SELECT DISTINCT
    O.SERVICETYPEOPTIONID AS id,
    O.SERVICETYPEOPTIONNAME COLLATE Cyrillic_General_CI_AS AS title,
    EL.SERVICEEXTRAID     AS 'extraLinkId',
    ACP.CHARGINGPOLICYID  AS 'chargingPolicyId',
    AO.OCCUPANCYTYPEID    AS 'capacityId',
    O.SERVICETYPETYPEID  AS 'optionClassId'
FROM
    SERVICE AS S
    INNER JOIN SERVICE_OPTION_IN_SERVICE      AS SO  ON SO.SERVICEID            = S.SERVICEID
    INNER JOIN SERVICE_TYPE_OPTION            AS O   ON O.SERVICETYPEOPTIONID   = SO.SERVICETYPEOPTIONID
    LEFT OUTER JOIN DEPENDENT_SERVICE_EXTRA   AS EL  ON EL.SERVICEOPTIONINSERVICEID  = SO.SERVICEOPTIONINSERVICEID
    LEFT OUTER JOIN ASSIGNED_OCCUPANCY        AS AO  ON AO.SERVICETYPEOPTIONID  = O.SERVICETYPEOPTIONID
    LEFT OUTER JOIN OCCUPANCY_TYPE            AS OT  ON OT.OCCUPANCYTYPEID      = AO.OCCUPANCYTYPEID
    LEFT OUTER JOIN ASSIGNED_CHARGING_POLICY  AS ACP ON ACP.SERVICETYPEOPTIONID = O.SERVICETYPEOPTIONID
WHERE
    S.SERVICEID = :productId
    AND SO.SERVICEOPTIONSTATUSID = 1
    AND (AO.OCCUPANCYTYPEID IS NULL OR OT.ACTIVE = 1)
