import sql from 'mssql';
import {register} from './events';
import {MSSQL_OPTIONS} from '../../../config';

export default async () => {

    let pool = null;

    const closePool = async () => {
        try {
            await pool.close();
            pool = null;
        } catch (err) {
            pool = null;
            throw new AppError({status: 400, ...err});
        }
    };

    const getConnection = async () => {
        try {

            if (pool) {
                return pool;
            }
            pool = await sql.connect(MSSQL_OPTIONS);

            pool.on('error', async err => {
                await closePool();
                throw new AppError({status: 400, ...err});
            });
            return pool;
        } catch (err) {
            pool = null;
            throw new AppError({status: 400, ...err});
        }
    };

    return register({sql, getConnection})
};
