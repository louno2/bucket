import fse from 'fs-extra';
import {join} from 'path';
import last from 'lodash/last';
import isNull from 'lodash/isNull';
import isString from 'lodash/isString';
import endsWith from 'lodash/endsWith';
import moment from 'moment';

const loadSqlQueries = async (folderName) => {

    const filePath = join(__dirname, folderName);
    const files = await fse.readdir(filePath);
    const sqlFiles = files.filter(f => f.endsWith(".sql"));
    const queries = {};
    for (let i = 0; i < sqlFiles.length; i++) {
        const query = fse.readFileSync(join(filePath, sqlFiles[i]), {encoding: "UTF-8"});
        queries[sqlFiles[i].replace(".sql", "")] = query;
    }
    return queries;
};

const prepareFormats = (rows, isModify = false) => {
    rows = rows.map(row => {
        for (let key in row) {
            if (row.hasOwnProperty(key)) {
                if (isString(row[key])) {
                    row[key] = row[key].trim();
                }

                if (isModify) {

                    /*if (!row[key]) {
                        row[key] = '—';
                    }*/

                    if (/ENCODING/.test(key)) {
                        let origKey = last(key.split('_'));
                        Reflect.deleteProperty(row, origKey);
                        row[origKey.toLowerCase()] = row[key];
                    } else if (endsWith(key, 'ID')) {
                        let [partKey,] = key.split('ID');
                        row[`${partKey.toLowerCase()}Id`] = row[key];
                    } else {
                        row[key.toLowerCase()] = (!/DATE/.test(key) || isNull(row[key]))
                            ? row[key] : moment(new Date(row[key])).format('DD.MM.YY')
                    }
                    Reflect.deleteProperty(row, key);
                }

            }
        }
        return row;
    });
    return rows
};

export {
    loadSqlQueries,
    prepareFormats
}
