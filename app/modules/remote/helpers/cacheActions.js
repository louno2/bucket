import {
    v5 as uuidv5,
    v4 as uuidv4
} from 'uuid';

const NAMESPACE = uuidv4();

export default {
    CACHE: {},
    currentKey: function (params) {
        return uuidv5(
            JSON.stringify(params),
            NAMESPACE
        );
    },
    set: function (params, value) {
        let key = this.currentKey(params);
        Object.assign(this.CACHE, {[key]: value});
        return value
    },
    get: function (params) {
        let key = this.currentKey(params);

        if (this.CACHE.hasOwnProperty(key)) {
            return this.CACHE[key];
        } else {
            return null;
        }
    }
}
