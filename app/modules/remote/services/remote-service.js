import superagent from 'superagent';
import isEmpty from 'lodash/isEmpty';
import compact from 'lodash/compact';
import isNull from 'lodash/isNull';
import isUndefined from 'lodash/isUndefined';
import AppError from '../../../helpers/appError';
import dbClient from '../manager';

import {
    JENGA_URL,
    OLYMP_URL,
    SEARCH_PARAMS
} from '../../../config';

class RemoteService {
    constructor() {
        (async () => {
            this.dbClient = await dbClient();
            return this;
        })();
    }

    async getBookings(params) {
        return this.dbClient.getBookings(params);
    }

    async getBookedData() {

        const bookedOptions = await wrapGetData(
            'BOOKEDPACKAGEID', 16990, 'getBookedOptions'
        );

        const bookedserviceIds = compact(bookedOptions
            .map(({bookedserviceId: id}) => id)
        );

        const costPriceIds = compact(bookedOptions
            .map(({originalcost_priceId: id}) => id)
        );

        const bookedServices = await this.dbClient.getDataByParams(
            true, 'BS.BOOKEDSERVICEID',
            bookedserviceIds, 'getBookedService'
        );

        const costPrices = await this.dbClient.getDataByParams(
            true, 'PR.PRICEID',
            costPriceIds, 'getPrice'
        );

        for (let bookedOption of bookedOptions) {

            const {bookedserviceId, originalcost_priceId} = bookedOption;
            const currentBookedService = bookedServices.find(service => (
                service.bookedserviceId === bookedserviceId
            ));
            const currentPrice = costPrices.find(({priceId}) => (
                originalcost_priceId === priceId
            ));
            Object.assign(bookedOption, {
                bookedservice: currentBookedService,
                priceCost: currentPrice
            });
        }

        return {bookedOptions};

    }

    async getBookingDataById(id, isModify = false) {

        //return await this.dbClient.getTemp();

        const wrapGetData = this.dbClient
            .getDataByParam.bind(this, isModify);


        const [info,] = await wrapGetData(
            'BOOKINGID', id, 'getBooking'
        );

        const [bookedPackage,] = await wrapGetData(
            'BOOKINGID', id, 'getBookedPackage'
        );

        const services = await wrapGetData(
            'BOOKINGID', id, 'getBookedServices'
        );

        let [client = {},] = await wrapGetData(
            'BOOKINGID', id, 'getClient'
        );

        if (!isEmpty(client)) {
            let {iscommisiontobeapproved: comission} = client;
            client.iscommisiontobeapproved = comission ? 'да' : 'нет';
        }

        const {packageId} = bookedPackage;

        const [packageData = {},] = await wrapGetData(
            'PACKAGEID', packageId, 'getPackage'
        );

        const options = await wrapGetData(
            'PACKAGEID', packageId, 'getPackageOptions'
        );

        let [flightService = {},] = await wrapGetData(
            'BOOKINGID', id, 'getBookedFlightService'
        );

        const passengers = await wrapGetData(
            'BOOKINGID', id, 'getPassenger'
        );

        const waitListPassengers = await wrapGetData(
            'BOOKINGID', id, 'getWaitListPassenger'
        );

        if (!isEmpty(flightService)) {
            let {supplierstatusId: status} = flightService;
            flightService.supplierstatus = status === 1
                ? 'активен' : 'не активен';
        }

        return {
            info, bookedPackage, packageData,
            client, passengers, services,
            options, waitListPassengers
        };
    }

    static async searchGeo(term) {

        let params = Object.assign({term}, SEARCH_PARAMS.geo);
        try {
            return await superagent
                .get(`${JENGA_URL}/searchGeo`)
                .set({'Content-Type': 'application/json'})
                .query(params);
        } catch (err) {
            throw new AppError({status: 500, ...err});
        }
    }

    static async searchProduct(params) {

        const {
            geoTitle, type,
            occupancy, withPrice,
            date, term = ''
        } = params;


        try {
            return await superagent
                .get(`${OLYMP_URL}/searchForBooking`)
                .query({
                    searchId: geoTitle,
                    settings: JSON.stringify({
                        types: [type], dateParams: date,
                        occupancy, withPrice
                    }),
                    type: 'productByGeo',
                    term
                });

        } catch (err) {
            console.log(err);
            throw new AppError({status: 500, ...err});
        }
    }

    async collectedProducts({products, ...params}) {

        let results = {};
        await Promise.all(products.map(product => (
            this.collectOptionsByProduct(params, product, results)
        )));
        return results;
    }

    async collectOptionsByProduct(params, product, results) {

        let {
            id: productId,
            iid: sourceId
        } = product;

        let {
            settings: {
                currencyCode,
                date, occupancy
            },
            term, type
        } = params;

        let optionParams = {
            searchId: productId,
            settings: !isNull(occupancy)
                ? JSON.stringify(occupancy) : '',
            type, term
        };

        let options;
        try {
            let {body} = await superagent
                .get(`${OLYMP_URL}/searchForBooking`)
                .set({'Content-Type': 'application/json'})
                .query(optionParams);
            options = body;
        } catch (err) {
            throw new AppError({status: 500, ...err});
        }

        let priceParams = JSON.stringify({
            currencyCode, date,
            occupancy: isNull(occupancy)
                ? {adult: 0, children: []}
                : occupancy
        });

        await Promise.all(options.map(option => (
            RemoteService.getPrice({
                option, params: priceParams,
                productId, sourceId
            })
        )));

        results[productId] = options;
        return Promise.resolve(results);
    }

    static async getPrice({option, params, productId, sourceId}, isPriceValue = true, eachDay) {

        let queryParams = {
            option: option.id,
            product: productId,
            source: sourceId,
            params
        };

        if (!isUndefined(eachDay)) {
            Object.assign(queryParams, {eachDay});
        }

        let priceData;
        try {
            let {body} = await superagent
                .get(`${OLYMP_URL}/calcBook`)
                .set({'Content-Type': 'application/json'})
                .query(queryParams);
            priceData = body;
        } catch (err) {
            throw new AppError({status: 500, ...err});
        }
        return isPriceValue ? Object.assign(option, {price: priceData}) : priceData;
    }
}

export default RemoteService;
