import isNull from 'lodash/isNull';
import cacheActions from '../helpers/cacheActions';
import RemoteService from '../services/remote-service';
const remoteService = new RemoteService();

export default {
    async getBookings(ctx) {

        const params = ctx.request.body;
        const data = await remoteService.getBookings(params);
        ctx.body = {bookingData: data}
    },
    async getBookingDataById(ctx) {

        const {id} = ctx.params;
        const {isModify} = ctx.request.query;

        const data = await remoteService
            .getBookingDataById(parseInt(id), isModify);

        ctx.body = {bookingData: data}
    },

    async searchGeo(ctx) {

        let {term = ''} = ctx.request.query;

        const {body: geoList} = await RemoteService.searchGeo(term);
        ctx.body = {geoList};
    },

    async searchProducts(ctx) {

        const params = ctx.request.body;

        let productList = [];
        let cacheData = cacheActions
            .get(params);

        if (!isNull(cacheData)) {
            ctx.body = { productList: cacheData};
        } else {
            const {body: response} = await RemoteService
                .searchProduct(params);
            cacheActions.set(params, response);
            ctx.body = { productList: response};
        }
    },

    async collectedProducts(ctx) {

        const params = ctx.request.body;
        const productList = await remoteService
            .collectedProducts(params);

        ctx.body = {productList};
    },

    async getPrice(ctx) {

        let params = ctx.request.query;
        let isEachDay = true;

        Object.assign(params, {
            option: {id: params.optionId}
        });

        const [priceDataSingle, priceDataByDays] = await Promise.all(
            [!isEachDay, isEachDay].map(boolValue =>
                RemoteService.getPrice(params, false, boolValue)
            ));

        ctx.body = {
            priceDataSingle,
            priceDataByDays
        }
    }
}
