import fs from 'fs';
import Router from 'koa-router';
import auth from './auth';
import users from './users';
import remote from './remote';
import bucket from './bucket';

import { DIST_DIR } from '../config';
import { IS_PROD } from '../utils/env';

const router = new Router();

router.use(auth);
router.use(users);
router.use(remote);
//router.use(bucket);

console.info(`${IS_PROD ? 'production' : 'development'} mode`);

if (IS_PROD) {

    const rootRouter = new Router();

    rootRouter.get('*', async(ctx) => {
        ctx.type = 'html';
        ctx.body = fs.createReadStream(`${DIST_DIR}/index.html`);
    });

    router.use('/',
        rootRouter.routes(),
        rootRouter.allowedMethods()
    );
}

export default router;
