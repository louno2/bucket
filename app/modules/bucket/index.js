import Router from 'koa-router';
import checkUser from '../../handlers/checkUser';
import bucketController from './controllers/bucket-controller';

const router = new Router({ prefix: '/api/buckets' });

router
    .get('/', checkUser(), bucketController.getList);

export default router.routes();
