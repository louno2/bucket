import React, { Component } from 'react'
import { EditorState, ContentState, convertFromHTML, convertToRaw } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
import draftToHtml from 'draftjs-to-html'
import PropTypes from 'prop-types'

const ToolbarOptions = {
  options: ['inline', 'blockType', 'list', 'textAlign', 'link', 'emoji', 'image', 'history'],
  inline: {
    options: ['bold', 'italic', 'underline'],
  },
}

export default class TextEditorTwo extends Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    initialValue: PropTypes.string.isRequired
  }

  constructor () {
    super()

    this.state = {
      editorState: EditorState.createEmpty(),
    }
    this.onEditorStateChange = this.onEditorStateChange.bind(this)
  }

  componentDidMount() {
    let { initialValue } = this.props;
    if (initialValue.length) {
      this.setState({
        editorState: EditorState.createWithContent(
          ContentState.createFromBlockArray(
            convertFromHTML(initialValue)
          )
        )
      })
    } else {
      this.setState({editorState: EditorState.createEmpty()})
    }
  }

  onEditorStateChange = (editorState) => {
    const {onChange} = this.props

    this.setState({
      editorState,
    })
    if (onChange) {
      onChange(draftToHtml(convertToRaw(editorState.getCurrentContent())))
    }
  }

  render () {
    const {editorState} = this.state
    return (
      <div className="text-editor">
        <Editor
          editorState={editorState}
          wrapperClassName="demo-wrapper"
          editorClassName="demo-editor"
          onEditorStateChange={this.onEditorStateChange}
          toolbar={ToolbarOptions}
        />
      </div>
    )
  }
}
