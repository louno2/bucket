import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import DatePicker, { registerLocale } from "react-datepicker";
import ru from "date-fns/locale/ru";

registerLocale('ru', ru);

class DatePickerField extends PureComponent {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    value: PropTypes.instanceOf(Date),
    allowableRange: PropTypes.array
  };

  constructor() {
    super();
    this.state = {
      value: null,
    };
    this.calendarRef = React.createRef();
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(date) {
    this.setState({value: date}, () => {
      this.props.onChange(date);
    });
  }

  componentDidMount() {

    this.calendarRef.current.setOpen(true);
    this.setState({value: this.props.value});
  }

  render() {
    const { value } = this.state;
    const [
        minDate, maxDate
    ] = this.props.allowableRange;

    return (
      <div className="date-picker">
        <DatePicker
          disabledKeyboardNavigation={true}
          minDate={minDate}
          maxDate={maxDate}
          className="form__form-group-datepicker"
          selected={value}
          onSelect={this.handleChange}
          dateFormat='dd.MM.yyyy'
          dropDownMode="select"
          locale="ru"
          ref={this.calendarRef}
        />
      </div>
    );
  }
}

const renderDatePickerField = (props) => {
  const { input } = props;
  return <DatePickerField {...input} />;
};

renderDatePickerField.propTypes = {
  input: PropTypes.shape({
    value: PropTypes.instanceOf(Date),
    allowableRange: PropTypes.array,
    onChange: PropTypes.func,
    name: PropTypes.string,
  }).isRequired,
};

export default renderDatePickerField;
