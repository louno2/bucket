/* eslint-disable no-param-reassign */
import React, { PureComponent } from 'react';
import isObject from 'lodash/isObject';
import isEqual from 'lodash/isEqual';

import DatePicker, { registerLocale } from "react-datepicker";
import ru from "date-fns/locale/ru";
registerLocale("ru", ru);

import MinusIcon from 'mdi-react/MinusIcon';
import PropTypes from 'prop-types';
import moment from 'moment';

class IntervalDatePickerField extends PureComponent {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      startDate: null,
      endDate: null
    };
    this.handleChange = this.handleChange.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { value } = nextProps;
    if (!isEqual(value, prevState) && isObject(value)) {
      return value;
    }
    else return null
  };

  handleChangeStart = startDate => this.handleChange({ startDate });

  handleChangeEnd = endDate => this.handleChange({ endDate });

  handleChange({ startDate, endDate }) {
    const { startDate: stateStartDate, endDate: stateEndDate } = this.state;

    const { onChange } = this.props;

    startDate = startDate || stateStartDate;
    endDate = endDate || stateEndDate;

    if (moment(startDate).isAfter(endDate)) {
      endDate = startDate;
    }
    this.setState({ startDate, endDate });
    onChange({ startDate, endDate });
  }

  render() {
    const { startDate, endDate } = this.state;

    return (
        <div className="date-picker date-picker--interval">
          <DatePicker
              locale="ru"
              selected={startDate}
              selectsStart
              startDate={startDate}
              endDate={endDate}
              onChange={this.handleChangeStart}
              dateFormat="dd.MM.yyyy"
              placeholderText="с"
              dropDownMode="select"
          />
          <MinusIcon className="date-picker__svg" />
          <DatePicker
              locale="ru"
              selected={endDate}
              selectsEnd
              startDate={startDate}
              endDate={endDate}
              onChange={this.handleChangeEnd}
              dateFormat="dd.MM.yyyy"
              placeholderText="по"
              dropDownMode="select"
          />
        </div>
    );
  }
}

const renderIntervalDatePickerField = (props) => {
  const { input } = props;
  return (
      <IntervalDatePickerField
          {...input}
      />
  );
};

renderIntervalDatePickerField.propTypes = {
  input: PropTypes.shape({
    onChange: PropTypes.func,
  }).isRequired,
};

export default renderIntervalDatePickerField;
