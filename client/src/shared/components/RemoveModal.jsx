import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Button, ButtonToolbar, Modal } from 'reactstrap';
import DeleteIcon from "mdi-react/DeleteIcon";

class RemoveModal extends PureComponent {

  static propTypes = {
    title: PropTypes.string,
    message: PropTypes.string,
    btnType: PropTypes.string.isRequired,
    actionFunc: PropTypes.func.isRequired,
    btnId: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };
  }

  toggle(action, event) {
    event.preventDefault();
    event.stopPropagation();

    if (action === 'approve') {
      this.props.actionFunc()
    }
    this.setState(prevState => ({ modal: !prevState.modal }));
  }

  render() {

    const {
      title, message,
      btnType, btnId = 'btn-remove'
    } = this.props;
    const { modal } = this.state;

    return (<Fragment>
          <Button
              onClick={this.toggle.bind(this, null)}
              color="danger" size="sm">
            <DeleteIcon/>
          </Button>
        <span onClick={(event) => {
          event.preventDefault();
          event.stopPropagation();
          }}>
        <Modal
          isOpen={modal}
          toggle={this.toggle.bind(this, null)}
          className="modal-dialog--danger theme-light ltr-support"
        >
          <div className="modal__header">
            <button className="lnr lnr-cross modal__close-btn"
                    type="button"
                    onClick={this.toggle.bind(this, null)} />
            <span className="lnr lnr-trash modal__title-icon" />
            <h4 className="modal__title">{title}</h4>
          </div>
          <div className="modal__body">
            {message}
          </div>
          <ButtonToolbar className="modal__footer">
            <Button size="sm"
                    className="modal_cancel"
                    onClick={this.toggle.bind(this, null)}>Отмена</Button>{' '}
            <Button size="sm"
                    className="modal_ok"
                    outline={false}
                    color='danger'
                    onClick={this.toggle.bind(this, 'approve')}>
              Выполнить
            </Button>
          </ButtonToolbar>
        </Modal>
        </span>
      </Fragment>
    );
  }
}

export default RemoveModal;
