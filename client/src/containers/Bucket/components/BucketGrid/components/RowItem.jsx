import React, { Fragment } from 'react';
import moment from 'moment';
import get from 'lodash/get';
import cn from 'classnames';
import {
    Button, UncontrolledTooltip
} from 'reactstrap';
import RemoveModal from '../../../../../../src/shared/components/RemoveModal';
import PlusCircleIcon from "mdi-react/PlusCircleIcon";
import DeleteIcon from "mdi-react/DeleteIcon";

const productTypesRu = {
    placement: 'размещение',
    excursion: 'экскурсия'
};

function RowItem(props) {

    const {
        date, isEmpty,
        removeFromGrid,
        chooseDayOnAdd,
        index
    } = props;



    let addBtn = (date) => <Fragment>
        <Button
            className={cn({ 'single-btn': isEmpty })}
            id={`add-btn-${index}`}
            onClick={() => chooseDayOnAdd(date)}
            color="primary" size="sm">
            <PlusCircleIcon/>
        </Button>
        <UncontrolledTooltip
            placement="top"
            target={`add-btn-${index}`}>
            Добавить на этот день
        </UncontrolledTooltip>
    </Fragment>;

    if (isEmpty) {

        return (
            <li className="main-row">
                <div className="heading">
                    <div className="col">
                        <p>{date}</p>
                    </div>
                    <div className="col">
                    </div>
                    <div className="col">
                    </div>
                    <div className="col">
                    </div>
                    <div className="col">
                    </div>
                    <div className="col">
                    </div>
                    <div className="col">
                        {addBtn(moment(date, 'DD.MM.YYYY').toDate())}
                    </div>
                </div>
            </li>)

    } else {

        const {
            occupancy: {adult, children},
            product: {title: productTitle},
            type: productType, hash,
            option: {title: optionTitle},
            fromDate, priceData = {}
        } = props;

        const currSymbolLabel = get(
            priceData, 'currency.symbol', ''
        );
        const [
            buyTotalLabel,
            sellTotalLabel
        ] = [
            'buyPrice.total',
            'total'
        ].map((field) => {
            let value = get(priceData, field, null);
            return value
                ? `${currSymbolLabel} ${value}`
                : 'По запросу';
        });

        return (
            <li className="main-row">
                <div className="heading">
                    <div className="col">
                        {date}
                    </div>
                    <div className="col">
                        <p>
                            {productTitle} ({optionTitle})
                        </p>
                    </div>
                    <div className="col">
                        взр. {adult} {children.length ? `, детск. ${children.length}` : null}
                    </div>
                    <div className="col">{productTypesRu[productType]}</div>
                    <div className="col">{buyTotalLabel}</div>
                    <div className="col">{sellTotalLabel}</div>
                    <div className="col">
                        <div id={`remove-btn-${index}`}>
                            <RemoveModal
                                btnType="btn-action"
                                color="danger"
                                actionFunc={removeFromGrid.bind(null, hash)}
                                title="Удаление продукта"
                                message={`Подтвердите удаление продукта «‎${productTitle} (${optionTitle})».`}
                            />
                        </div>
                        <UncontrolledTooltip
                            placement="top"
                            target={`remove-btn-${index}`}>
                            Удалить
                        </UncontrolledTooltip>
                        {addBtn(moment(fromDate).toDate(), hash)}
                    </div>
                </div>
            </li>
        )

    }
}

export default RowItem;
