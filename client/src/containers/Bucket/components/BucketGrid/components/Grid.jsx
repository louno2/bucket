import React, { Fragment } from 'react';
import RowItem from './RowItem';
import Alert from '../../../../../shared/components/Alert';

function Grid({ data, columns, removeFromGrid, chooseDayOnAdd }) {

    let elementColumns = columns.map((item, index) => {
        return (<div key={index}
                     className="hcol">{item.label}</div>);
    });

    let elementRows = data.map((item, index) => {
        return (<RowItem
            index={index}
            key={index} {...item}
            chooseDayOnAdd={chooseDayOnAdd}
            removeFromGrid={removeFromGrid}/>);
    });

    return (
            <div className="bucket-grid-wrap">
                {elementRows.length ?
                    <Fragment>
                        <div className="table-data">
                            <div className="header">{elementColumns}</div>
                            <ul className="main-row-wrap">{elementRows}</ul>
                        </div>
                    </Fragment>
                    : <Alert color="info" className="alert--bordered" icon>
                        <p>Начните добавлять продукты для их вывода.</p>
                    </Alert>
                }
            </div>
    );
}

export default Grid;
