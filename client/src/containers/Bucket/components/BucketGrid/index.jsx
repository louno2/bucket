import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import Grid from './components/Grid';
import { Button, ButtonToolbar } from 'reactstrap';
import { calcTotalSum } from '../../utils';
import SmartArea from "../SmartArea";

import { GRID_COLUMNS } from '../../utils/constants';

class BucketGridWrap extends PureComponent {

    static propTypes = {
        rows: PropTypes.array.isRequired,
        createCommercialOffer: PropTypes.func.isRequired,
        removeFromGrid: PropTypes.func.isRequired,
        chooseDayOnAdd: PropTypes.func.isRequired,
        duration: PropTypes.number.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            tableHeader: null,
            tableHeaderFixed: false
        };
    };

    render() {

        const {
          rows = [],
          removeFromGrid,
          chooseDayOnAdd, duration
        } = this.props;

        return (
            <Fragment>
                <Grid
                   duration={duration}
                   data={rows}
                   columns={GRID_COLUMNS}
                   removeFromGrid={removeFromGrid}
                   chooseDayOnAdd={chooseDayOnAdd}
                   headerFixed={this.state.tableHeaderFixed}
                />
                <ButtonToolbar>
                    <Button onClick={this.props.createCommercialOffer}
                         disabled={!rows.length}
                         size="sm"
                         color="success">
                        Создать предложение
                    </Button>
                </ButtonToolbar>
            </Fragment>
        );
    }
};

export default BucketGridWrap
