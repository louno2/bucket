import React, {Fragment} from 'react';
import moment from 'moment';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import {
    Col, Card, CardBody
} from 'reactstrap';

export default function BucketHead({ totalSumLabel, presetData : PD }) {
    return (
        <Fragment>
            <Col md={12} xl={8} lg={6} xs={12}>
                <Card>
                    <CardBody className="bucket-info">
                        <ul className="cells-wrap">
                            <li>
                                <div className="sub-head">Заезд</div>
                                <div>{moment(PD.fromDate).format('DD.MM.YYYY')}</div>
                            </li>
                            <li>
                                <div className="sub-head">Выезд</div>
                                <div>{moment(PD.toDate).format('DD.MM.YYYY')}</div>
                            </li>
                            <li>
                                <div className="sub-head">Ночей</div>
                                <div>{PD.duration}</div>
                            </li>
                            <li>
                                <div className="sub-head">Взрослые</div>
                                <div>{get(PD, 'occupancy.adult', 0)}</div>
                            </li>
                            <li>
                                <div className="sub-head">Дети</div>
                                <div>{get(PD, 'occupancy.children', []).join(',') || 0}</div>
                            </li>
                        </ul>
                    </CardBody>
                </Card>
            </Col>
            <Col md={12} xl={2} lg={6} xs={12}>
                <Card>
                    <CardBody className="dashboard__card-widget">
                        <div className="card__title">
                            <h5 className="bold-text">Итоговая цена</h5>
                        </div>
                        <div className="dashboard__total">
                            <p dangerouslySetInnerHTML={{__html: totalSumLabel}}
                                className="dashboard__total-stat"></p>
                        </div>
                    </CardBody>
                </Card>
            </Col>
        </Fragment>
    )
};

BucketHead.propTypes = {
    totalSumLabel: PropTypes.string.isRequired,
    presetData: PropTypes.object.isRequired
};
