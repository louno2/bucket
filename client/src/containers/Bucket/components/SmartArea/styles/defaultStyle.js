export default {

    control: {
        backgroundColor: '#fff',
        fontSize: 14,
        fontWeight: 'normal',
    },

    highlighter: {
        overflow: 'hidden',
        lineHeight: '27px',
        letterSpacing: '2px'
    },

    input: {
        margin: 0,
    },

    '&singleLine': {
        control: {
            display: 'inline-block',
            width: 130,
        },

        highlighter: {
            padding: 1,
            border: '2px inset transparent',
        },

        input: {
            padding: 1,
            border: '2px inset',
        },
    },

    '&multiLine': {
        highlighter: {
            padding: 9,
        },

        input: {
            padding: 9,
            height: 120,
            outline: 0,
            border: 0,
            cursor: 'text'
        },
    }
}
