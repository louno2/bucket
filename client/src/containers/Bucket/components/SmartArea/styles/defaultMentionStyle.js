export default {
    backgroundColor: 'hsla(208, 97%, 62%, 0.76)',
    paddingTop: '3px',
    paddingBottom: '3px',
    color: '#fff',
    position: 'relative',
    zIndex: 1,
    pointerEvents: 'none'
}
