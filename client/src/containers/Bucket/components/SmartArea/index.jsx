import React, {PureComponent, Fragment} from 'react';
import PropTypes from 'prop-types';
import cloneDeep from 'lodash/cloneDeep';
import isNull from 'lodash/isNull';
import isUndefined from 'lodash/isUndefined';
import get from 'lodash/get';
import moment from 'moment';
import cn from 'classnames';
import {
    Container, Col, Row,
    Card, CardBody, Badge,
    Button, ButtonToolbar
} from 'reactstrap';

import {
    Mention,
    MentionsInput
} from 'react-mentions';

import defaultStyle from './styles/defaultStyle';
import defaultMentionStyle from './styles/defaultMentionStyle';

import DatePicker from '../../../../shared/components/form/DatePicker';
import CheckBox from '../../../../shared/components/form/CheckBox';
import Alert from '../../../../shared/components/Alert';
import Validator from '../../utils/assemblyValidator';

import {
    fetchData,
    TopPartNotificatior,
    StringInteraction,
    DataPresetter,
    TextAreaAccessor,
    ProductPrices,
    checkingDisplayedKeywords
} from '../../utils';


import {
    ASSEMBLY_REQUIRED_FIELDS,
    KEY_WORDS_REGEXP,
    KEY_WORDS,
    KEY_WORDS_LIST,
    SERVICE_TYPES,
    MAX_ROWS_COUNT,
    ENTER_KEY,
    HINTS
} from '../../utils/constants';

class SmartArea extends PureComponent {

    static propTypes = {
        addToGrid: PropTypes.func.isRequired,
        backToGrid: PropTypes.func.isRequired,
        params: PropTypes.object.isRequired,
        currency: PropTypes.object.isRequired,
        seatings: PropTypes.array.isRequired
    };

    constructor(props) {

        super(props);

        this.prodPrices = new ProductPrices();
        let fromDate = moment();
        let toDate = moment().add(20, 'days');

        const dataList = [
            fetchData.bind(this, 'geo'),
            [{id: fromDate.format(), display: fromDate.format('DD.MM.YYYY')}],
            [{id: toDate.format(), display: toDate.format('DD.MM.YYYY')}],
            SERVICE_TYPES, [],
            fetchData.bind(this, 'product'),
            fetchData.bind(this, 'option')
        ];

        this.state = {
            isEdit: false,
            withPrice: false,
            value: '',
            assembly: this.prodPrices.assembly,
            priceData: {},
            occupInfo: [],
            messageBody: {
                type: null,
                text: null,
                labelRu: null,
                isShow: false
            },
            dates: [
                fromDate.toDate(),
                toDate.toDate()
            ],
            mentionsData: [{
                trigger: '@',
                markup: "{{__display__}}",
                data: KEY_WORDS.accessory.concat(KEY_WORDS.main),
                type: 'util',
                addSpace: false,
                render: this.showOptions('util'),
                isAvailble: true
            }].concat(KEY_WORDS_LIST.map(({
                 id: type, literal, isSkip
               }, index) => ({
                    trigger: `${literal}:`,
                    data: dataList[index],
                    markup: `|:${literal}:{{__display__}}`,
                    render: this.showOptions(type),
                    type, addSpace: true,
                    isAvailble: true
                })
            ))
        };

        Validator.setTargetObject(
            this.state.assembly
        );
        this.inputRef = React.createRef();
    }

    setProperty = (fieldName, value) => {

        setTimeout(this.setState
            .bind(this, {isEdit: true})
        );

        if (fieldName === 'util') return;

        const { cacheList } = this.prodPrices;
        const {
            assembly : prevAssembly
        } = cloneDeep(this.prodPrices);

        switch (fieldName) {
            case 'occupancy':
                let occupancy = this.state.occupInfo
                    .find(({id}) => id === value).value;
                this.prodPrices.assignValue(
                    'assembly.occupancy',
                    { value :cloneDeep(occupancy)}
                );
                break;
            case 'product':
                const product = cloneDeep(cacheList.products
                    .find(({id}) => id === value));
                for (let args of [
                    ['assembly.product',  { value: product }],
                    ['cacheList.options', { value: product.options}]
                ]) this.prodPrices.assignValue(...args);
                break;
            case 'option':
                const option = cacheList.options
                    .find(({id}) => id === value);
                this.prodPrices.assignValue(
                    'assembly.option', { value: option }
                );
                break;
            default:
                this.prodPrices.assignValue(
                    `assembly.${fieldName}`, { value }
                );
                break;
        }

        const {
            assembly : currentAssembly
        } = cloneDeep(this.prodPrices);

        this.setState({assembly: currentAssembly}, () => {
            checkingDisplayedKeywords.call(this);
            Validator.checkTargetObject.call(this);
            StringInteraction.setValue.call(this, {
                type:'onAdd', fieldName,
                prevValue: prevAssembly[fieldName]
            });
            this.checkAssemblyAndGetPrice();
        });

    };


    onChange(event) {

        let { value } = event.target;

        const {
          getListOnDelete,
          clearProperties
        } = StringInteraction;

        for (let {pattern, word} of KEY_WORDS_REGEXP) {
            value = value.replace(pattern, word);
        }

        let { assembly } = this.state;
        let listOnDel = getListOnDelete
            .call(this, value);

        let modifiedAssembly = clearProperties
            .call(this, listOnDel);

        this.setState({ value, assembly: modifiedAssembly }, () => {
            if (listOnDel.length) {
                checkingDisplayedKeywords.call(this);
                Validator.checkTargetObject.call(this);
                StringInteraction.setValue.call(this, {
                    type:'onDel', fieldNames: listOnDel
                });
            }
            this.inputRef.current.focus();
        });
    }

    checkAssemblyAndGetPrice = (onlyCheck = false) => {

        const { assembly } = this.prodPrices;

        const isReadyAssembly = ASSEMBLY_REQUIRED_FIELDS.every(
            field => !isNull(assembly[field])
        );

        if (!onlyCheck) {
            if (isReadyAssembly) {
                fetchData.call(this, 'price', null,
                    ({priceDataSingle: val}) => {
                        this.setState({priceData: (isNull(val) || isUndefined(val)) ? {} : val})
                    }
                );
            } else {
                this.setState({ priceData: {} });
            }
        } else return isReadyAssembly;
    };

    setDateValue = (type, dateValue) => {

        const { literal } = KEY_WORDS_LIST
            .find(({ id }) => id === type);

        const { value, assembly } = this.state;

        const isReseted = Object.keys(assembly)
            .every(field => isNull(assembly[field]));

        let modifiedValue = !isReseted ? get(
            StringInteraction.collectParts({
                type: 'onAdd',
                fieldName: type,
                value: dateValue
                }, value.trim(), assembly
            ), 'parts.str'
        ) : '';
        let dateStr = moment(dateValue)
            .format('DD.MM.YYYY');
        modifiedValue = `${modifiedValue} |:${literal}:{{${dateStr}}}`.trim();

        this.setState({value: modifiedValue}, () => {
            this.setProperty(type, dateValue);
            this.inputRef.current.focus();
        });
    };

    showOptions(type) {
        return (
            suggestion, search,
            highlightedDisplay,
            index, focused
        ) => {
            const {
                dates, assembly
            } = this.state;

            switch (type) {
                case 'fromDate':
                case 'toDate':
                    const [ fromDate, toDate ] = dates;
                    return (<div className="date-picker"
                                 onClick={(e) => e.stopPropagation()}>
                        <DatePicker input={{
                            allowableRange: [fromDate, toDate],
                            value: type === 'fromDate' ? fromDate : toDate,
                            onChange: this.setDateValue.bind(this, type),
                            name: type
                        }}/>
                    </div>);
                case 'util':
                    let { display, id : fieldName } = suggestion;
                    const isSelected = !isNull(assembly[fieldName]);
                    return (<div className={`list-item ${focused ? 'focused' : ''} ${!index ? 'first-elem' : ''}`}>
                        {display}
                        <Badge className={cn('hint-label', isSelected
                            ? 'selected'
                            : 'not-selected' )}>
                            {isSelected ? 'задано' : 'выбрать'}
                        </Badge>
                    </div>);
                default: {
                    let {display} = suggestion;
                    return (<div className={`list-item ${focused ? 'focused' : ''} ${!index ? 'first-elem' : ''}`}>
                        {highlightedDisplay}
                    </div>);
                }
            }
        };
    }

    addToGrid = () => {
        const onlyCheck = true;
        const isReadyAssembly = this
            .checkAssemblyAndGetPrice(onlyCheck);
        if (isReadyAssembly) {
            let {assembly, priceData} = this.state;
            Object.assign(assembly, {priceData});

            this.setState({
                priceData: {},
                assembly: {},
                value: '',
            }, () => {
                this.props.addToGrid(assembly);
                this.prodPrices.resetInitialValue();
                this.props.backToGrid();
                DataPresetter.setData.call(this);
            });
        }
    };

    componentDidMount() {
        TopPartNotificatior.switchOn();
        TextAreaAccessor.eventListener(this, 'add');
        TextAreaAccessor.setRestrict.call(this);
        DataPresetter.setData.call(this);
        checkingDisplayedKeywords.call(this);
        Validator.checkTargetObject.call(this);
    }

    componentWillUnmount() {
       TopPartNotificatior.switchOff();
       TextAreaAccessor.eventListener(this, 'remove');
    }

    render() {

        const {
            total, currency
        } = get(this.state, 'priceData', {});
        const { withPrice, messageBody } = this.state;

        return (
            <Fragment>
                <div className="smart-area-wrap">
                    <Container>
                        <Row>
                            <Col md={7} sm={12}>
                                <Card className="grid">
                                    <CardBody>
                                        <h4 className="cart__total">
                                            Итоговая цена: {!isUndefined(total) ? `${currency.symbol} ${total}` : 'по запросу'}
                                        </h4>
                                        <CheckBox
                                            label="Поиск только с ценой"
                                            onChange={() => {
                                                let value = !withPrice;
                                                this.setState({withPrice: value},
                                                    this.setProperty.bind(
                                                        this, 'withPrice', value
                                                    ));
                                            }}
                                            name="checkbox-with-price"
                                            className="colored-click"
                                            defaultChecked={!withPrice}
                                            value={withPrice}
                                        />
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col md={5} sm={12}>
                                <Card className="grid">
                                    <CardBody>
                                        {(() => {
                                           const {
                                               isShow, text,
                                               type, subhead
                                           } = messageBody;
                                           return isShow && (<Alert color={type} className="alert--bordered" icon>
                                                <p><span className="bold-text">{subhead}: </span>{text}</p>
                                            </Alert>)
                                        })()}
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12}>
                                <Card className="grid">
                                    <CardBody>
                                        <MentionsInput
                                            value={this.state.value}
                                            onChange={this.onChange.bind(this)}
                                            style={defaultStyle}
                                            inputRef={this.inputRef}
                                            allowSpaceInQuery={true}
                                            allowSuggestionsAboveCursor={true}
                                            placeholder={"Для вывода списка начните с '@'"}>
                                            {this.state.mentionsData
                                                .filter(({ isAvailble }) => isAvailble)
                                                .map(({
                                                trigger, markup, data,
                                                type, render, addSpace
                                             }, index) =>
                                                <Mention
                                                    key={index}
                                                    trigger={trigger}
                                                    markup={markup}
                                                    data={data}
                                                    appendSpaceOnAdd={addSpace}
                                                    renderSuggestion={render}
                                                    onAdd={this.setProperty.bind(this, type)}
                                                    style={defaultMentionStyle}/>)
                                            }
                                        </MentionsInput>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </div>
                <ButtonToolbar>
                    <Button onClick={this.addToGrid} size="sm" color="success">
                        Добавить в корзину
                    </Button>
                </ButtonToolbar>
            </Fragment>
        )
    }
}

export default SmartArea;
