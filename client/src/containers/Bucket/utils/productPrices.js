import moment from 'moment';
import _ from 'lodash';

export const assemblyFields = [
    ['geo', 'Гео'],
    ['fromDate', 'Дата заезда'],
    ['toDate', 'Дата выезда'],
    ['type', 'Тип'],
    ['occupancy', 'Рассадка'],
    ['product', 'Продукт'],
    ['option', 'Опция']
];

export default class ProductPrices {

    constructor() {
        this.resetInitialValue();
    }

    resetInitialValue(immutableFields = []) {

        this.__cacheList = {
            products: [],
            options: []
        };
        this.__currency = {};

        let prevAssembly = _.cloneDeep(this.__assembly);

        this.__assembly = assemblyFields
          .map(([fieldName,]) => fieldName)
          .reduce((store, field) => {
            if (!~immutableFields.indexOf(field)) {
              store[field] = null;
            }
            return store;
        }, prevAssembly || {});
    }

    assignValue(key, { value, isReset = false}) {

        const prefixKey = `__${key}`;

        if (isReset) {
            return _.set(this, prefixKey, null)
        }

        switch (key) {
            case 'params': Object.assign(this, value); break;
            case 'assembly.option':
                _.set(this, prefixKey, _.pick(value, [
                    'id', 'title'
                ])); break;
            case 'assembly.product':
                _.set(this, prefixKey, _.pick(value, [
                   'id', 'title', 'source',
                ]));
                _.set(this, 'assembly.option', null);
                break;
            case 'assembly.geo':
                const [ id, title ] = value.split(':');
                _.set(this, prefixKey, {
                    id: parseInt(id), title
                  });
                break;
            default:
                _.set(this, prefixKey, value)
        }
    }

    set assembly(values) {
        Object.assign(this.__assembly, values);
    }

    set currency(value) {
        Object.assign(this.__currency, value);
    }

    get assembly() {
        return this.__assembly;
    }

    get cacheList() {
        return this.__cacheList;
    }

    genParams() {

        const {
            type, geo, occupancy,
            product, option,
            fromDate, toDate,
            withPrice = false
        } = this.assembly;

        const { __currency : { code } } = this;
        const geoTitle = geo.title;
        const date =  {
            start: moment(fromDate)
                .format('YYYY-MM-DD'),
            duration: moment(toDate)
                .diff(fromDate, 'd')
        };

        return (typeObject, products = [], extendedData) => {
            switch (typeObject) {
                case 'productList':
                    return {
                        ...extendedData,
                        type, geoTitle,
                        date, occupancy,
                        withPrice
                    };
                case 'productFullList':
                    return {
                        ...extendedData,
                        type: `option${occupancy ? 'ByCapacity' : ''}`,
                        products: products.map(({id, source: {iid}}) => ({id, iid})),
                        settings: { occupancy,  date, currencyCode: code }
                    };
                case 'price':
                    return {
                        productId: product.id,
                        optionId: option.id,
                        sourceId: _.get(product, 'source.iid'),
                        params: { occupancy,  date, currencyCode: code }
                    }
            }
        }
    }

    static saturateProductsWithOptions(dataOptions, occupancy, products) {

        for (let productId in dataOptions) {

            let options = dataOptions[productId];
            let optsWithCap = options.filter(option => option.optType === 'main');
            let minPrice = ProductPrices.calcMinPrice(optsWithCap);
            if (occupancy) ProductPrices.checkAddPlacement(optsWithCap, occupancy);
            let product = products.find(({id}) => id === productId);

            Object.assign(product, {
                labelPrice: minPrice ? `от ${minPrice.total} ${minPrice.code}` : 'По запросу',
                options
            });
        }
        return Promise.resolve(products.filter(({options}) => options.length));
    }

    static checkAddPlacement(options, {adult: OcupAdult, children}) {

        for (let option of options) {

            Object.assign(option, {moreTitle: '', addPlacements: []});

            let {adult: CapAdult, child} = option.capacity;
            let {exbed, exchd} = option;

            if (CapAdult.max < OcupAdult && exbed._id) {
                if (exbed._id) {
                    option.moreTitle = `+ ${exbed.title}`;
                    option.addPlacements.push({
                        id: exbed._id,
                        occupancy: {adult: OcupAdult - CapAdult.max, children: []}
                    });
                }
            }

            if (child.max < children.length && exchd._id) {
                if (exchd._id) {
                    option.moreTitle = `+ ${exchd.title}`;
                    option.addPlacements.push({
                        id: exchd._id,
                        occupancy: {adult: 0, children: children.slice(0, children.length - child.max)}
                    });
                }
            }
        }
    }

    static calcMinPrice(options) {
        return _.chain(options).map(({price}) => {
            if (_.get(price, 'currency', false)) {
                let {total, currency: {code}} = price;
                return {total, code};
            } else return null;
        }).compact().minBy('total').value();
    }
}
