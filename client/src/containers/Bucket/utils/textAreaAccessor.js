import $ from 'jquery';
import {ENTER_KEY, MAX_ROWS_COUNT} from './constants';

export default {
    eventListener: function(self, action) {
        document[`${action}EventListener`](
            'click', this.handleClick.bind(self)
        )
    },
    handleClick: function(event) {
        const {current} = this.inputRef;
        let isEdit = current && current.contains(event.target);
        this.setState({isEdit});
    },
    setRestrict: function() {
        let self = this;
        $('textarea').bind('keypress', function (event) {

            if (!self.state.isEdit) {
                event.preventDefault();
            }
            let textarea = $(this);
            let text = textarea.val();
            let numberOfLines = (text.match(/\n/g) || []).length + 1;
            if (event.which === ENTER_KEY && numberOfLines === MAX_ROWS_COUNT) {
                return false;
            }
        });
    }
};
