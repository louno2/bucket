import isEmpty from 'lodash/isEmpty';
import {
    collectedProducts,
    getPrice,
    searchGeo,
    searchProducts
} from '../../../utils/bucketRequests';
import ProductPrices from "./productPrices";

export default function fetchData(type, query, callback) {

    switch (type) {
        case 'geo':

            return searchGeo({term: query})
                .then(data => data.map(
                    ({title: display, id}) => ({id: `${id}:${display}`, display})
                ))
                .then(callback);

        case 'product': {

            const genParams = this.prodPrices.genParams();
            let params = genParams('productList', [], { term: query });
            const { occupancy } = params;

            for (let field of ['products', 'options'])
                this.prodPrices.assignValue(`cacheList.${field}`,  { value:[]});

            return searchProducts(params)
                .then(products => {

                    const params = genParams(
                        'productFullList', products
                    );

                    return collectedProducts(params)
                        .then(options => ProductPrices
                            .saturateProductsWithOptions(
                                options, occupancy, products
                            )
                        )
                })
                .then(products => {
                    this.prodPrices.assignValue(
                        'cacheList.products', { value: products }
                    );
                    return products.map(product => ({
                        id: product.id,
                        display: `${product.title} (${product.labelPrice})`,
                    }));
                })
                .then(callback)
        }
        case 'option': {

            const { options } = this.prodPrices.cacheList;
            const pattern = new RegExp(query, 'i');

            return Promise.resolve(options
                .filter(({ title }) => pattern.test(title))
                .map(({ id, title, price = {} }) => {
                    let priceLabel = '';
                    if (!isEmpty(price)) {
                        priceLabel =  `(${price.total} ${price.currency.code})`;
                    }
                    return { id, display: `${title} ${priceLabel}`};
                })
            ).then(callback)
        }
        case 'price' : {

            const params = this.prodPrices
                .genParams()('price');

            return getPrice(params)
                .then(callback)
        }
    }
}
