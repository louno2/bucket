import cloneDeep from 'lodash/cloneDeep';
import chain from 'lodash/chain';
import keyBy from 'lodash/keyBy';
import mapValues from 'lodash/mapValues';

const MAX_ROWS_COUNT = 4;
const ENTER_KEY = 13;

const ASSEMBLY_REQUIRED_FIELDS = [
    'geo', 'type', 'product',
    'fromDate', 'toDate',
    'occupancy', 'option'
];

const SERVICE_TYPES = [
    {id: 'placement', display: 'Размещение'},
    {id: 'excursion', display: 'Экскурсия'}
];

const KEY_WORDS_LIST = [
        {
            id: 'geo',
            display: 'Гeo',
            literal: 'G',
            type: 'accessory',
            priority: true
        }, {
            id: 'fromDate',
            display: 'Заезд',
            literal: 'A',
            type: 'accessory',
            priority: false
        }, {
            id: 'toDate',
            display: 'Выезд',
            literal: 'D',
            type: 'accessory',
            priority: false
        }, {
            id: 'type',
            display: 'Тип',
            literal: 'T',
            type: 'accessory',
            priority: true
        }, {
            id: 'occupancy',
            display: 'Рассадка',
            literal: 'S',
            type: 'accessory',
            priority: false
        }, {
            id: 'product',
            display: 'Продукт',
            literal: 'P',
            type: 'main',
            priority: true
        }, {
            id: 'option',
            display: 'Опция',
            literal: 'O',
            type: 'main',
            priority: true
        }
];

const KEY_WORDS = ['main', 'accessory']
    .reduce((store, type) => {
        store[type] = KEY_WORDS_LIST
            .filter(w => w.type === type)
            .map(({ id, display, literal, priority }) => ({
                id,
                display: `${display} (${literal})`,
                literal,
                priority
             }));
        return store;
    }, {});

const KEY_WORDS_REGEXP = KEY_WORDS_LIST.map(({display, literal}) => ({
    pattern: new RegExp(`\{\{${display} \\(${literal}\\)\}\}`, 'ig'),
    word: `${literal}:`
}));

const HINTS = {
    letterLiteral: 'Соответствие буквенных литералов с данными выпадающих списков: G - гео, A - заезд, D - выезд, T - тип, S - рассадка, P - продукт, O - опция (после символа добавлять «:»)'
};

const GRID_COLUMNS = [
    { label: 'Дата' },
    { label: 'Название' },
    { label: 'Рассадка' },
    { label: 'Тип' },
    { label: 'Цена покупки' },
    { label: 'Цена продажи' },
    { label: '' }
];

const TEXT_AREA_OPTIONS = {
   styles: {
       hidden: {
           visibility: 'hidden',
           opacity: '0',
       },
       showing: {
           visibility: 'visible',
           opacity: '1',
           transition: 'visibility 0s linear 0s, opacity 150ms'
       }
   },
   delays: {
       beforeHiding: 250,
       flicker: 150,
       wordRestrictor: 550
   }
};

const SWIPEABLE_STYLES = {
    tabs: {
        background: '#fff',
    },
    slide: {
        padding: 15,
        minHeight: 100,
    },
    slide1: {
        backgroundColor: '#fff'
    },
    slide2: {
        backgroundColor: '#fff'
    }
};

export {
    TEXT_AREA_OPTIONS,
    ASSEMBLY_REQUIRED_FIELDS,
    SWIPEABLE_STYLES,
    KEY_WORDS_REGEXP,
    SERVICE_TYPES,
    KEY_WORDS,
    KEY_WORDS_LIST,
    MAX_ROWS_COUNT,
    ENTER_KEY,
    GRID_COLUMNS,
    HINTS
}
