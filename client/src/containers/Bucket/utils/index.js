import ProductPrices from './productPrices';
import DataPresetter from './dataPresetter';
import TextAreaAccessor from './textAreaAccessor';
import TopPartNotificatior from './topPartNotificatior';
import StringInteraction from './stringInteraction';
import fetchData from './fetchData';
import swipeableOpts from './swipeableOpts';
import checkingDisplayedKeywords from './checkingDisplayedKeywords'

export {
    swipeableOpts,
    fetchData,
    StringInteraction,
    DataPresetter,
    TextAreaAccessor,
    TopPartNotificatior,
    checkingDisplayedKeywords,
    ProductPrices
};
