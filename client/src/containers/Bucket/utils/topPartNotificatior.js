import $ from "jquery";
import showNotification from "../../Common/Notification";
import {HINTS} from "./constants";

export default {
    switchOff: () => $('.rc-notification-notice-content').hide(),
    showing: () => showNotification({
        message: HINTS.letterLiteral ,
        type: 'primary'
    }, 'full'),
    switchOn: function () {
        this.switchOff();
        this.showing();
    }
};
