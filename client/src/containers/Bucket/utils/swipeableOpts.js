import { SWIPEABLE_STYLES } from './constants';

export default {
    styles: SWIPEABLE_STYLES,
    genStyles: function(type, currentIndex) {

        let condition; let index;
        switch (type) {
            case 'bucketGridSwipe':
                condition = !currentIndex;
                index = 1;
                break;
            case 'smartAreaSwipe':
                condition = currentIndex;
                index = 2;
                break;
        }
        return Object.assign({
                visibility: condition ? 'visible' : 'hidden',
                opacity: condition ? 1 : 0,
                transition: 'visibility 0.1s linear,opacity 0.1s linear',
                marginTop: '10px'
            },
            this.styles.slide,
            this.styles[`slide${index}`]
        );
    }
};
