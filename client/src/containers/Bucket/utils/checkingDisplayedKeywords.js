import isNull from 'lodash/isNull';
import {
    KEY_WORDS,
    TEXT_AREA_OPTIONS as  TAO
} from './constants';

export default function() {

    const {
     assembly : targetObj,
     mentionsData : mentionsList
   } = this.state;

   const isFullSet = KEY_WORDS.accessory.every(
       ({ id : fieldName }) => !isNull(targetObj[fieldName])
   );

   const isExistsValue = KEY_WORDS.main.some(
    ({ id : fieldName }) => !isNull(targetObj[fieldName])
   );

   const utilItem = mentionsList
       .find(({ type }) => type === 'util');

   utilItem.data = KEY_WORDS.accessory.concat(
      isFullSet ? KEY_WORDS.main : []
   );


   setTimeout(this.setState.bind(this, {
       mentionsData: mentionsList
           .map(({ isAvailble, type, ...rest }) => ({
                   ...rest, type,
                   isAvailble: !isFullSet && !isExistsValue ? !KEY_WORDS.main
                       .map(({ id }) => id).includes(type) : true
               })
           )
   }), TAO.delays.wordRestrictor);


}
