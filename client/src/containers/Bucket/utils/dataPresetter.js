import moment from 'moment';
import { seatingFormat } from '../../../utils/seating';
import pick from 'lodash/pick';
import StringInteraction from './stringInteraction';

export default (() => {

    function setDateParams(dataDates) {

        let { mentionsData } = this.state;
        let [ fromDateMention, toDateMention] = mentionsData
            .filter(({ type }) => /date/i.test(type));
        let mentionsDates = { fromDateMention, toDateMention };

        for (let key in dataDates) {
            let value = dataDates[key];
            Object.assign(mentionsDates[`${key}Mention`],  { data: [{
                    id: moment(value).format(),
                    display: moment(value).format('DD.MM.YYYY')
                }]});
        }

        this.setState({
            mentionsData,
            dates: Object.values(dataDates)
        });
    };

    function actionsOnOccupancy() {

        let { mentionsData, occupInfo } = this.state;
        let occupMention = mentionsData
            .find(({ type }) => type === 'occupancy');

        return {
            setList({ occupancy }) {

                let { adult, children } = occupancy;
                let {
                    occupId, label
                } = seatingFormat(
                    adult, children
                );

                occupMention.data.push({
                    id: occupId,
                    display: label
                });
                occupInfo.push({
                    id: occupId,
                    value: occupancy
                });

            },
            expandList(seatings) {

                for (let seating of seatings) {

                    let { title, occupInfo : info } = seating;

                    let occupMatched = occupMention.data
                        .find(({ id }) => id === info.id);

                    if (!occupMatched) {
                        occupMention.data.push({
                            id: info.id,
                            display: title
                        });
                        occupInfo.push({
                            id: info.id,
                            value: info.value
                        });
                    }
                }
            },
            applyChanges() {
                this.setState({ mentionsData, occupInfo });
            }
        }
    }

    return {
        setData: function() {

            const {
                params, currency, seatings
            } = this.props;

            Object.assign(this.prodPrices, {
                assembly: params,
                currency: currency
            });

            setDateParams.call(this, pick(
                params, ['fromDate', 'toDate']
            ));

            let {
                setList,
                expandList,
                applyChanges
            } = actionsOnOccupancy.call(this);

            setList(pick(params, 'occupancy'));
            expandList(seatings);
            applyChanges.call(this);
            StringInteraction.setValue
                .call(this, {}, true, params);
        }
    }
})()
