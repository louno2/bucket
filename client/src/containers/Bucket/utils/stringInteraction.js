import moment from 'moment';
import get from 'lodash/get';
import isNull from 'lodash/isNull';
import cloneDeep from 'lodash/cloneDeep';
import isEqual from 'lodash/isEqual';

import compact from 'lodash/fp/compact';
import split from 'lodash/fp/split';
import sortBy from 'lodash/fp/sortBy';
import uniqBy from 'lodash/fp/uniqBy';
import flow from 'lodash/fp/flow';
import filter from 'lodash/fp/filter';
import map from 'lodash/fp/map';
import reverse from 'lodash/fp/reverse';

import {
    TEXT_AREA_OPTIONS as TAO,
    KEY_WORDS, KEY_WORDS_LIST
} from './constants';

function getListOnDelete(value) {

    let assembly = get(
        this.prodPrices,
        'assembly', {}
    );

    return KEY_WORDS_LIST
        .filter(({ literal, id : fieldName }) => {
            let pattern = new RegExp(`\\|\:${literal}\:`, 'g');
            return !pattern.test(value) && !isNull(assembly[fieldName]);
        })
        .map(({ id }) => id);
}

function clearProperties(listOnDel) {

    let copyAssembly = cloneDeep(
        this.state.assembly
    );

    if (listOnDel.length) {
        for (let prop of listOnDel) {
            let current = get(this.prodPrices, `assembly.${prop}`, null);
            if (!isNull(current)) {
                this.prodPrices.assignValue(
                    `assembly.${prop}`,  { isReset: true }
                );
            }
        }
        if (listOnDel.includes('product')) {
            for (let prop of ['products', 'options'])
                this.prodPrices.assignValue(
                    `cacheList.${prop}`, { value: [] }
                );
        }

        for (let prop of listOnDel) {
            copyAssembly[prop] = null;
        }
    }

    return copyAssembly;
}

function collectLiterals({ type : action , fieldNames = [], fieldName, prevValue }, targetObj) {

    let listLiterals = KEY_WORDS.accessory
        .concat(KEY_WORDS.main)
        .map(({ literal }) => literal);

    const [
        main, accessory
    ] = ['main', 'accessory']
      .reduce((store, fieldName) => {
         let list = KEY_WORDS[fieldName]
             .filter(({ priority : p }) => p);
         store.push({
            fieldNames: list.map(({ id }) => id),
            literals: list.map(({ literal }) => literal)
         });
        return store;
    }, []);

    const [
        productLiteral,
        optionLiteral
    ] = ['P', 'O'].map(literal =>
        main.literals.find(l => l === literal)
    );

    const [
        productFieldName,
        optionFieldName
    ] = ['product', 'option'].map(fieldName =>
        main.fieldNames.find(f => f === fieldName)
    );

    if (action === 'onAdd') {

        const checks = {
            isChangedAccessory: accessory.fieldNames
                .includes(fieldName),
            isFilledAtLeastMain: main.fieldNames.some(
                field => !isNull(targetObj[field])
            ),
            isProduct: fieldName === main.fieldNames
                .find(f => f === 'product'),
            isSameValue: isEqual(targetObj[fieldName], prevValue)
        };

        switch (true) {
            case checks.isProduct
                && !checks.isSameValue: {
                return [ false, listLiterals.filter(
                    literal => optionLiteral !== literal
                )];
            }
            case checks.isChangedAccessory
                && checks.isFilledAtLeastMain
                && !checks.isSameValue : {
                    return [false, listLiterals.filter(
                        literal => !main.literals.includes(literal)
                    )]
            }
            default:
                return [ false, listLiterals ];
        }
    }

    if (action === 'onDel') {

        const checks = {
            isRemoveAccessory: accessory.fieldNames.some(
                fieldName => fieldNames.includes(fieldName)
            ),
            isRemoveProduct: !!fieldNames.find(fieldName => (
                productFieldName === fieldName
            )),
            isFilledOption: !isNull(targetObj[optionFieldName])
        };

        switch (true) {
            case checks.isRemoveAccessory: {
                return [ true,
                    listLiterals.filter(
                    literal => (![
                        productLiteral,
                        optionLiteral
                    ].includes(literal))
                )];
            }
            case checks.isRemoveProduct && checks.isFilledOption: {
                return [ true,
                    listLiterals.filter(
                    literal => optionLiteral !== literal
                )];
            }
            default:
                return [
                  false,
                  listLiterals
                ];
        }
    }
}

function collectParts(actionInfo, strValue, targetObj) {

    const [
        isGroupDel,
        listLiteralValues
    ] = collectLiterals(
        actionInfo, targetObj
    );

    const literals = {
        list: listLiteralValues,
        condition: function(part) {
            let marker = this.list.find(marker => {
                let pattern = new RegExp(`:${marker}:`, 'i');
                return pattern.test(part);
            });
            return this.list.indexOf(marker);
        },
        maskCompare: function (part) {
            let pattern = /\|:([A-Z]+):\{\{(.*)\}\}/;
            return `|${part}`.match(pattern);
        },
        filterByValue: function (data) {
            const currLiteral = data[1];
            return listLiteralValues.includes(currLiteral)
        }
    };

    return {
        parts: {
            str: flow(
                split('|'), reverse, compact,
                sortBy(literals.condition.bind(literals)),
                uniqBy(literals.condition.bind(literals)),
                map(literals.maskCompare),
                filter(literals.filterByValue),
                map(([value,]) => value)
            )(strValue),
            currentOrder: flow(
                split('|'), compact,
                map(literals.maskCompare),
                filter(literals.filterByValue),
                map(data => data[1])
            )(strValue)
        },
        literals, isGroupDel
    };
};

function setValue(actionInfo, isReset = false, presetData) {

    if (isReset) {

        const partsOfStrValue = {
            geo: ':G:{{}}',
            fromDate: ':A:{{}}',
            toDate: ':D:{{}}',
            occupancy: ':S:{{}}'
        };

        this.setState({
            value: Object.keys(partsOfStrValue)
                .reduce((store, key, index) => {
                    if (presetData.hasOwnProperty(key)) {
                        let part = partsOfStrValue[key];
                        let value;
                        switch (key) {
                            case 'fromDate':
                            case 'toDate': {
                                value = moment(presetData[key])
                                    .format('DD.MM.YYYY');
                                break
                            }
                            case 'geo': {
                                value = presetData[key].title;
                                break;
                            }
                            case 'occupancy': {
                                let {adult, children} = presetData[key];
                                let childLabel = children.length > 0 ? `+ ${children.length} детск.` : '';
                                value = `взр. ${adult} ${childLabel}`;
                            }
                        }
                        if (!index) part = '|' + part;
                        store.push(part.replace(/{}/g, `{${value}}`));
                    }
                    return store;
                }, []).join('\xa0|').trim() + '\xa0'
        });
    } else {

        let { value, assembly } = this.state;

        const {
            parts, literals,
            isGroupDel
        } = collectParts(
            actionInfo,
            value.trim(),
            assembly
        );

        let parentWrapStyle = get(this.inputRef,
            'current.parentElement.style'
        );


        const checkChangeOrder = () => {

            let isDiffPos = false;
            let currentLiterals = literals.list
              .filter(literal => {
                let { id : fieldName } = KEY_WORDS_LIST
                  .find((w) => w.literal === literal);
                return !isNull(assembly[fieldName])
            });
            let isSubMatched = !!~currentLiterals.join()
                .indexOf(parts.currentOrder.join());

            for (let i=0; i < parts.currentOrder.length; i++ ) {
                if (parts.currentOrder[i] !== literals.list[i])  {
                    isDiffPos = true;
                    break;
                }
            }
            return isDiffPos && !isSubMatched;
        };

        const isChangeOrder = checkChangeOrder();
        const modifiedValue = `${parts.str.join(' ')}\xa0`;

        const applyChanges = () => {
            this.setState({ value: modifiedValue }, () => {
                let listOnDel = getListOnDelete
                    .call(this, modifiedValue);
                if (listOnDel.length) {
                    let modifiedAssembly = clearProperties
                        .call(this, listOnDel);
                    this.setState({assembly: modifiedAssembly});
                }
            })
        };

        if (isChangeOrder || isGroupDel) {
            setTimeout(() => {
                Object.assign(parentWrapStyle, TAO.styles.hidden);
                setTimeout(() => {
                    Object.assign(parentWrapStyle, TAO.styles.showing);
                    applyChanges();
                    this.inputRef.current.focus();
                }, TAO.delays.flicker);
            }, TAO.delays.beforeHiding);

        } else {
            applyChanges()
        }
    }
}

export default {
    setValue,
    collectParts,
    getListOnDelete,
    clearProperties
}
