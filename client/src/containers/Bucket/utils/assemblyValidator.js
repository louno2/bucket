import moment from 'moment';
import isNull from 'lodash/isNull';
import { assemblyFields } from './productPrices';

export default (() => {

    let targetObj = null;

    const mapMessages = {
        warning: 'Предупреждение',
        info: 'Подсказка'
    };

    const getBody = (checkType, value) => {

        switch (checkType) {
            case 'dates': {
                return {
                    type: 'warning',
                    text: 'Дата заезда следует за датой выезда'
                }
            }
            case 'fullness': {
                return {
                    type: 'info',
                    text: `для сборки заполнить поля - ${value.join(', ').toLowerCase()}`
                }
            }
        }
    };

    const validator = {
        checkDates: function ({fromDate, toDate}) {

            const condition = [toDate, fromDate].some(
                date => !moment(date).isValid()
            ) ? true : moment(toDate).isAfter(fromDate);

            return {
                body: getBody('dates'),
                condition
            };
        },
        checkFullness: function () {

            let results = [];
            for (let [fieldName, labelRu] of assemblyFields) {
                if (isNull(targetObj[fieldName])) {
                    results.push(labelRu);
                }
            }

            return {
                condition: !results.length,
                body: getBody('fullness', results)
            }
        }
    };

    const methods = Object.keys(validator);


    return {
        setTargetObject: (data) => {
            targetObj = data;
        },
        checkTargetObject: function() {

            let result = {
                messageBody: {
                    isShow: false
                }
            };

            for (let method of methods) {
                let {
                    condition, body
                } = validator[method](targetObj);

                if (!condition) {
                    result = {
                        messageBody: {
                            subhead: mapMessages[body.type],
                            isShow: true, ...body
                        }
                    };
                    break;
                }
            }

            this.setState(result)
        }
    }
})();
