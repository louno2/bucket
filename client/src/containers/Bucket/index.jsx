import React, {PureComponent, Fragment} from 'react';
import {connect} from 'react-redux';
import {compose} from 'redux';
import cn from 'classnames';
import PropTypes from 'prop-types';

import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import isNumber from 'lodash/isNumber';
import isNull from 'lodash/isNull';
import pick from 'lodash/pick';
import isEqual from 'lodash/isEqual';

import SwipeableViews from 'react-swipeable-views';
import {
    Col, Container, Row,
    Card, CardBody,
    Nav, NavItem, NavLink,
    TabContent, TabPane
} from 'reactstrap';

import { withLoadingScreen } from '../Common/LoadingScreenManager';
import BucketGrid from './components/BucketGrid';
import SmartArea from './components/SmartArea';
import BucketHead from './components/BucketHead';
import Alert from '../../shared/components/Alert';

import {
    addToGrid,
    removeFromGrid,
    getPresetData
} from '../../redux/actions/bucketActions';

import {
    createCommercialOffer,
    saveSeatings
} from '../../utils/crmRequests';

import {
    calcTotalSum,
    swipeableOpts
} from './utils';

class Bucket extends PureComponent {

    static propTypes = {
        gridRows: PropTypes.array.isRequired,
        currency: PropTypes.object.isRequired,
        addToGrid: PropTypes.func.isRequired,
        getPresetData: PropTypes.func.isRequired,
        removeFromGrid: PropTypes.func.isRequired,
        seatings: PropTypes.array.isRequired,
        products: PropTypes.array.isRequired,
        crmParams: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            datesParams: {},
            ownUpdate: false
        };
        this.loaderToggle = get(
            props.loadingScreen, 'toggle'
        );
    }

    changeTab = (value) => {
        this.setState({
            index: value,
            ownUpdate: true
        });
    };

    createData = () => {

        let {
            crmParams: { leadId },
            presetData, currency,
            products, seatings
        } = this.props;


        createCommercialOffer(
            leadId, products,
            presetData, currency,
            this.loaderToggle
        ).then(orderId => {
            if (isNumber(orderId) && seatings.length) {
              saveSeatings(
                 orderId, seatings,
                 this.loaderToggle
              );
            }
        });
    };

    calcTotalSum = (rows = [], { symbol }) => {
        let result = 0;
        for (let row of rows) {
            let rowTotal =  get(row, 'priceData.total', 0);
            result += rowTotal;
        }
        return result > 0 ? `${symbol} ${result}` : 'По запросу';
    };

    chooseDayOnAdd = (fromDate) => {

        const { toDate } = this.props.presetData;
        this.setState({
          datesParams: { fromDate, toDate },
          index: 1, ownUpdate: true
        });
    };

    static getDerivedStateFromProps (nextProps, prevState) {

        let datesParams = pick(
            nextProps.presetData, [
            'fromDate', 'toDate'
        ]);

        if (prevState.ownUpdate) {
            return {
                datesParams: prevState.datesParams,
                index: prevState.index,
                ownUpdate: false
            }
        } else if (!isEqual(
            datesParams,
            prevState.datesParams
        )) {
            return { datesParams }
        }
        else return null
    }

    render() {

        const {
            index, datesParams
        } = this.state;

        const {
            presetData, currency,
            gridRows, addToGrid,
            removeFromGrid, seatings
        } = this.props;

        const { duration } = presetData;

        const totalSumLabel = this.calcTotalSum(
            gridRows, currency
        );

        const params = Object.assign({},
            datesParams, pick(presetData, [
                'geo', 'occupancy'
             ]));

        return (
            <Container className="dashboard bucket-dashboard">
                <Row>
                    <Col md={12}>
                        <h3 className="page-title">Корзина</h3>
                    </Col>
                </Row>

                {!duration ? (<Row>
                        <Col md={12}>
                            <Card>
                                <CardBody>
                               <Alert color="warning" className="alert--bordered" icon>
                                        <p><span className="bold-text">Предупреждение: </span>
                                            Дата заезда и дата выезда одна и та же, для вывода корзины необходим
                                            не пустой диапазон дат.
                                        </p>
                               </Alert>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>)
                 : (<Fragment>

                    <Row className="bucket-head">
                        <BucketHead
                            presetData={presetData}
                            totalSumLabel={totalSumLabel}/>
                    </Row>
                    <Row>
                        <Col md={12}>
                            <Card>
                                <CardBody className="bucket-body">
                                    <div className="bucket-wrap">
                                        <div className="tabs tabs--bordered-bottom">
                                            <div className="tabs__wrap">
                                                <Nav tabs>
                                                    <NavItem>
                                                        <NavLink
                                                            className={cn({active: !index})}
                                                            onClick={this.changeTab.bind(this, 0)}>
                                                            Список продуктов
                                                        </NavLink>
                                                    </NavItem>
                                                    <NavItem>
                                                        <NavLink
                                                            className={cn({active: !!index})}
                                                            onClick={this.chooseDayOnAdd.bind(
                                                                this, presetData.fromDate
                                                            )}>
                                                            Добавление продукта
                                                        </NavLink>
                                                    </NavItem>
                                                </Nav>
                                                <TabContent>
                                                    {(!isEmpty(presetData)) ?
                                                        <SwipeableViews index={index}>
                                                            <div style={swipeableOpts.genStyles(
                                                                'bucketGridSwipe', index
                                                            )}>
                                                                {(index === 0) &&  <BucketGrid
                                                                    chooseDayOnAdd={this.chooseDayOnAdd}
                                                                    createCommercialOffer={
                                                                        this.createData.bind(this)
                                                                    }
                                                                    removeFromGrid={removeFromGrid}
                                                                    duration={presetData.duration}
                                                                    rows={gridRows}/>}
                                                            </div>
                                                            <div style={swipeableOpts.genStyles(
                                                                'smartAreaSwipe', index
                                                            )}>
                                                                {(index === 1) && <SmartArea
                                                                    seatings={seatings}
                                                                    currency={currency}
                                                                    params={params}
                                                                    backToGrid={this.changeTab.bind(this, 0)}
                                                                    addToGrid={addToGrid}/>}
                                                            </div>
                                                        </SwipeableViews> : null}
                                                </TabContent>
                                            </div>
                                        </div>
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Fragment>)}


            </Container>
        )
    }
}

const mapStateToProps = ({bucket, seating, crmParams}) => {

    let {
        gridRows, products,
        currency, presetData
    } = bucket;

    let {
        seatings
    } = seating;

    return {
        currency, presetData,
        products, seatings,
        crmParams, gridRows
    }
};

export default compose(
    connect(mapStateToProps, {
        addToGrid,
        getPresetData,
        removeFromGrid
    }),
    withLoadingScreen
)(Bucket);
