import React from 'react';
import PropTypes from 'prop-types';
import ImageWorksModal from './components/ImageWorksModal';

class DropImagesWrap extends React.Component {

  static propTypes = {
    callback: PropTypes.func.isRequired
  }

  render () {
    return (
     <React.Fragment>
        <ImageWorksModal
          title="Загрузка изображения"
          message=""
          btnIntro="Выбрать файл"
          btnCreate="Выполнить"
          btnCancel="Убрать"
          icon="lnr-file-add"
          callback={this.props.callback}>
        </ImageWorksModal>
     </React.Fragment>
    )
  }
}

export default DropImagesWrap
