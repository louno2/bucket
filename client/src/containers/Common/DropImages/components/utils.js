import axios from 'axios';
import showNotification from '../../../Common/Notification';

const {API_URL = ''} = process.env;

function uploadImages ({files}) {

  let [file,] = files
  let data = new FormData()
  data.append('file', file)

  this.setState({isUploading: true}, () => {
    return new Promise((resolve, reject) => {
      axios
        .post(`${API_URL}/api/media/upload`, data, {
          onUploadProgress: (progressEvent) => {
            let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
            this.setState({progress: percentCompleted})
          }
        })
        .then(({data: { hash, isUploaded, filename }}) => {

          if (isUploaded) {
            setTimeout(() => {
              this.setState({
                progress: 0,
                isUploading: false
              })
              this.props.callback(hash, filename);
              this.toggle();
              showNotification({
                type:'success',
                message:'Изображение загружено на сервер.'
              })
              resolve()
            }, 1000)
          }
        })
        .catch(err => {
          console.error(err)
          this.setState({isUploading: false})
          showNotification({
            type:'danger',
            message:'Произошла ошибка при загрузке изображения на сервер.'
          })
        })
    })
  })
}

export {
  uploadImages
}
