import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import pick from 'lodash/pick';
import cn from 'classnames';
import {
    Progress, Button,
    ButtonToolbar, Modal
} from 'reactstrap';

import { uploadImages } from './utils'
import DropImage from './DropImage';

export default class ImageWorksModal extends PureComponent {

  static propTypes = {
      btnCancel: PropTypes.string.isRequired,
      btnCreate: PropTypes.string.isRequired,
      btnIntro: PropTypes.string.isRequired,
      callback: PropTypes.func.isRequired,
      message: PropTypes.string,
      title: PropTypes.string.isRequired
    };

    constructor() {
        super();
        this.state = {
            isOpen: false,
            isUploading: false,
            progress: 0
        };
    }

    toggle = () => {
        this.setState(prevState => ({ isOpen: !prevState.isOpen }));
    }

    render() {

      const {
        title, message, btnIntro
      } = this.props;

      const { isOpen, progress, isUploading } = this.state;

        return (<div className="btn-load-image-wrap">
            <Button color='success' size="sm" onClick={this.toggle}>{btnIntro}</Button>
            <Modal
                isOpen={isOpen}
                toggle={this.toggle}
                className="modal-dialog--success right-part-modal theme-light ltr-support"
            >
                <div className="modal__header">
                    <button className="lnr lnr-cross modal__close-btn" type="button" onClick={this.toggle} />
                    <h4 className="modal__title">{title}</h4>
                    <h5 className="modal_subhead">Используемые форматы: jpeg, png</h5>
                </div>
                <div className="modal__body">
                    {progress > 0 ? (<div className="modal-progress-wrap">
                          <div className="progress-wrap progress-wrap--small">
                            <Progress animated value={progress}/>
                          </div>
                       </div>) : null}
                    <div className={cn({'modal-drop-image-wrap': isUploading})}>
                        <DropImage { ...pick(this.props, ['btnCreate', 'btnCancel']) } onSubmit={uploadImages.bind(this)}/>
                    </div>
                </div>
                <div className="modal__footer">
                </div>
            </Modal>
        </div>)
    }
}
