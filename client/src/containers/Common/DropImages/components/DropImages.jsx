import React from 'react';
import {
   Button, ButtonToolbar,
} from 'reactstrap';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import renderDropZoneMultipleField from '../../../../shared/components/form/DropZoneMultiple';

const DropFiles = ({ handleSubmit, reset }) => {
  return (
        <form className="form" onSubmit={handleSubmit}>
          <Field
            name="files"
            component={renderDropZoneMultipleField}
          />
          <ButtonToolbar className="form__button-toolbar">
            <Button color="primary" type="submit">Загрузить</Button>
            <Button type="button" onClick={reset}>
              Отмена
            </Button>
          </ButtonToolbar>
        </form>
      );
};

DropFiles.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired
};

export default reduxForm({
  form: 'drop_files_form', // a unique identifier for this form
})(DropFiles)
