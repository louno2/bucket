import React, { Fragment } from 'react';
import {
  Card, CardBody, Col, Button, ButtonToolbar,
} from 'reactstrap';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import renderDropZoneField from '../../../../shared/components/form/DropZone';

const FileUploadDefault = ({ handleSubmit, reset, btnCreate, btnCancel }) => {
  return (<Fragment>
    <form className="form" onSubmit={handleSubmit}>
          <Field
            name="files"
            component={renderDropZoneField}
          />
          <ButtonToolbar className="modal__footer downward__shift">

            <Button  size="sm" onClick={reset}>{btnCancel}</Button>
            <Button color="success" size="sm" type="submit">{btnCreate}</Button>

            {/*<button className="button button-cancel" type="button" onClick={reset}>{btnCancel}</button>
            <button className="button button-success" type="submit">{btnCreate}</button>*/}
          </ButtonToolbar>
        </form>
    </Fragment>)
};

FileUploadDefault.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired
};

export default reduxForm({
  form: 'file_upload_default', // a unique identifier for this form
})(FileUploadDefault);
