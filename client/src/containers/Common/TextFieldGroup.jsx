import React from 'react';
import PropTypes from 'prop-types';
import AccountOutlineIcon from 'mdi-react/AccountOutlineIcon';

const TextFieldGroup = ({
    type, name, error,
    placeholder, label,
    value, onChange, disabled,
    children, icon : Icon,
    bottomLink : BottomLink,
    rightBtn: RightBtn
}) => {
    return (
        <div className="form__form-group">
            {label.length ? (<span className="form__form-group-label">{label}</span>) : null}
            <div className="form__form-group-field">
                {Icon ? (<div className="form__form-group-icon">
                    <Icon/>
                </div>) : ''}
                <div className="form__form-group-input-wrap form__form-group-input-wrap--error-above">
                    <input
                        placeholder={placeholder}
                        name={name}
                        type={type}
                        value={value}
                        onChange={onChange}
                        disabled={disabled}
                    />
                    {error && <span className="form__form-group-error">{error}</span>}
                </div>
                { RightBtn ? <RightBtn/> : null }

            </div>
            { BottomLink ? <BottomLink/> : null }
        </div>
    );
}

TextFieldGroup.propTypes = {
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.string.isRequired,
    info: PropTypes.string,
    error: PropTypes.string,
    type: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    disabled: PropTypes.string,
    icon: PropTypes.object,
    bottomLink: PropTypes.func,
    rightBtn: PropTypes.func
};

TextFieldGroup.defaultProps = {
    type: 'text'
};


export default TextFieldGroup;
