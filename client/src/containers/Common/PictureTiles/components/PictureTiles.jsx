/* eslint-disable react/no-array-index-key */
import React, { PureComponent, Component } from 'react';
import PropTypes from 'prop-types';
import Lightbox from 'react-images';

export default class PictureTiles extends Component  {
  static propTypes = {
    images: PropTypes.arrayOf(PropTypes.shape({
      src: PropTypes.string,
      alt: PropTypes.string,
    })).isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,
      images: [],
      lightboxIsOpen: false,
      currentImage: 0,
    };
  }

  openLightbox = (index, event) => {
    event.preventDefault();
    this.setState({
      currentImage: index,
      lightboxIsOpen: true,
    });
  };

  closeLightbox = () => {
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false,
    });
  };

  gotoPrevious = () => {
    this.setState(prevState => ({ currentImage: prevState.currentImage - 1 }));
  };

  gotoNext = () => {
    this.setState(prevState => ({ currentImage: prevState.currentImage + 1 }));
  };

  gotoImage = (index) => {
    this.setState({
      currentImage: index,
    });
  };

  handleClickImage = () => {
    const { currentImage } = this.state;
    const { images } = this.props;
    if (currentImage === images.length - 1) return;
    this.gotoNext();
  };

  toggleModal = () => {
    this.setState(state => ({ modalIsOpen: !state.modalIsOpen }));
  };

  static getDerivedStateFromProps(nextProps, prevState){
    if(nextProps.images!==prevState.images){
      return { images: nextProps.images};
    }
    else return null;
  }

  render() {
    const {
      currentImage, lightboxIsOpen,
      images, modalIsOpen
    } = this.state;

    return (
      <div className="picture-tiles">
        {images.map((img, i) => (
          <a className="picture-tiles__img-wrap" key={i} onClick={e => this.openLightbox(i, e)} href={img.src}>
            <img src={img.src} alt={img.alt} />
          </a>
        ))}
        <Lightbox
          currentImage={currentImage}
          images={images}
          isOpen={lightboxIsOpen}
          onClickImage={this.handleClickImage}
          onClickNext={this.gotoNext}
          onClickPrev={this.gotoPrevious}
          onClickThumbnail={this.gotoImage}
          onClose={this.closeLightbox}
        />
      </div>
    );
  }
}
