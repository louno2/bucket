import React, { Component } from 'react';
import isEmpty from 'lodash/isEmpty';
import clone from 'lodash/clone';
import PropTypes from 'prop-types';
import PictureTiles from './components/PictureTiles';
import DropImagesWrap from '../../Common/DropImages';
import get from 'lodash/get';

const { API_URL = '' } = process.env;
const API_IMG_URL = `${API_URL}/public/original/`;

class PictureTilesWrap extends Component {

  static propTypes = {
    title: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    cachePageData: PropTypes.func.isRequired,
    initialValues: PropTypes.object.isRequired,
  }

   state = {
      images: []
   }

   getFileNames = (imgHash, filename) => {
    let {
      cachePageData, type,
      initialValues
    } = this.props;
    this.setState({images: [{
      src: `${API_IMG_URL}${filename}`,
      alt: ''
    }]}, () => {
      cachePageData({
        images: Object.assign({}, initialValues, {[type]: imgHash} )
      });
    });
  }

  componentDidMount () {
    this.setState(prevState => {

      const { type } = this.props;
      const value = this.props.initialValues[type];

      switch (type) {
        case 'anonsCard':
          prevState.images = isEmpty(value) ? [] : [{
            src: `${API_IMG_URL}${value.filename}`,
            alt: value.alt
          }]
          break;
        default:
          prevState.images = []
      }
      return prevState;
    })
  }

  render() {

    let { images } = this.state;
    let { type } = this.props;

    return (
      <div className="block-panel">
        <div className="block-panel-title">
          {this.props.title}
        </div>
        <div className="block-panel-body">
          { images.length ? <PictureTiles images={images}/> : null }
          <DropImagesWrap callback={this.getFileNames}/>
        </div>
    </div>)
  }
};

export default PictureTilesWrap;
