import React from 'react';
import NotificationSystem from 'rc-notification';
import {
  BasicNotification,
  FullWideNotification
} from '../../shared/components/Notification';

let notification = null;

NotificationSystem.newInstance({ style: { top: 65 } }, n => notification = n);

const titles = {
  success: 'Успешно',
  warning: 'Предупреждение',
  danger: 'Ошибка',
};

export default function showNotification({ message, type }, width = 'short') {

  switch (width) {
    case 'short':
      notification.notice({
        content: <BasicNotification
            color={type}
            title={titles[type]}
            message={message}
        />,
        duration: 5,
        closable: true,
        style: { top: 0, left: 'calc(100vw - 100%)' },
        className: `right-up ltr-support`,
      });
      break;
    case 'full':
      notification.notice({
        content: <FullWideNotification
            color={type}
            message={message}
        />,
        duration: 45,
        maxCount: 1,
        closable: true,
        style: { top: 0, left: 0 },
        className: 'full',
      });
      break;
  }


};
