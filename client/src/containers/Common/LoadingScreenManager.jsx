import React, { Component } from 'react';
import showNotification from '../Common/Notification';
const { Consumer, Provider } = React.createContext();

export class LoadingProvider extends Component {
    state = {
        loading: true,
        loaded: true,
    };

    toggle = (value, notify = null) => {
        if (value) {
            this.setState({
                loading: true,
                loaded: false
            })
        } else {
            this.setState({loading: false});
            setTimeout(() => this.setState({loaded: true}), 200);
        }
        if (notify) {
            showNotification(notify, 'short');
        }
    };

    render() {
        const context = {
            toggle: this.toggle
        };

        let {loaded, loading} = this.state;

        return (
            <Provider value={context}>
                {!loaded
                && (
                    <div className={`load${loading ? '' : ' loaded'}`}>
                        <div className="load__icon-wrap">
                            <svg className="load__icon">
                                <path fill="#4ce1b6" d="M12,4V2A10,10 0 0,0 2,12H4A8,8 0 0,1 12,4Z"/>
                            </svg>
                        </div>
                    </div>
                )
                }
                {this.props.children}
            </Provider>
        );
    }
}

export const LoadingConsumer = ({children}) => (
    <Consumer>{context => children(context)}</Consumer>
);

export const withLoadingScreen = Comp => props => (
    <LoadingConsumer>
        {context => <Comp loadingScreen={context} {...props} />}
    </LoadingConsumer>
);
