import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import Topbar from './topbar/Topbar';
import Sidebar from './sidebar/Sidebar';
import { compose } from 'redux';

import { changeThemeToDark, changeThemeToLight } from '../../redux/actions/themeActions'
import { changeMobileSidebarVisibility, changeSidebarVisibility } from '../../redux/actions/sidebarActions'
import { SidebarProps } from '../../shared/prop-types/ReducerProps'
import { logoutUser } from '../../redux/actions/authActions';


class Layout extends Component {
  static propTypes = {
    sidebar: SidebarProps.isRequired,
    logoutUser: PropTypes.func.isRequired
  };

  onLogout = (e) => {
    e.preventDefault()
    this.props.logoutUser()
  };

  changeSidebarVisibility = () => {
    this.props.changeSidebarVisibility()
  };

  changeMobileSidebarVisibility = () => {
    this.props.changeMobileSidebarVisibility()
  };

  changeToDark = () => {
    this.props.changeThemeToDark()
  };

  changeToLight = () => {
    this.props.changeThemeToLight()
  };

  render () {
    const {
      sidebar, auth: {user}, location,
    } = this.props;

    const layoutClass = classNames({
      layout: true,
      'layout--collapse': sidebar.collapse,
    });

    return (
      <div className={layoutClass}>
        <Topbar
          user={user}
          changeMobileSidebarVisibility={this.changeMobileSidebarVisibility}
          changeSidebarVisibility={this.changeSidebarVisibility}
          onLogout={this.onLogout}
        />
        <Sidebar
          sidebar={sidebar}
          changeToDark={this.changeToDark}
          changeToLight={this.changeToLight}
          changeMobileSidebarVisibility={this.changeMobileSidebarVisibility}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  sidebar: state.sidebar
});

export default compose(
  connect(mapStateToProps, {
    logoutUser,
    changeSidebarVisibility,
    changeMobileSidebarVisibility,
    changeThemeToDark,
    changeThemeToLight
  }),
  withRouter,
)(Layout);
