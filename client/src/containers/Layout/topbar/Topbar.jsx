import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import TopbarSidebarButton from './TopbarSidebarButton';
import TopbarProfile from './TopbarProfile';

class Topbar extends PureComponent {
  static propTypes = {
    user: PropTypes.object.isRequired,
    changeMobileSidebarVisibility: PropTypes.func.isRequired,
    changeSidebarVisibility: PropTypes.func.isRequired,
    onLogout: PropTypes.func.isRequired
  };

  render() {
    const {
      changeMobileSidebarVisibility,
      changeSidebarVisibility,
      onLogout, user
    } = this.props;

    return (
      <div className="topbar">
        <div className="topbar__wrapper">
          <div className="topbar__left">
            <TopbarSidebarButton
              changeMobileSidebarVisibility={changeMobileSidebarVisibility}
              changeSidebarVisibility={changeSidebarVisibility}
            />
          {/*  <Link className="topbar__logo" to="/dashboard_default" />*/}
          </div>
          <div className="topbar__right">
            <TopbarProfile
                user={user}
                onLogout={onLogout}  />
          </div>
        </div>
      </div>
    );
  }
}

export default Topbar;
