import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import DownIcon from 'mdi-react/ChevronDownIcon';
import {Collapse} from 'reactstrap';
import TopbarMenuLink from './TopbarMenuLink';
import { Link } from 'react-router-dom';

//const Ava = `${process.env.PUBLIC_URL}/img/ava.png`;
import Ava from '../../../assets/images/ava.png';
import SidebarLink from "../sidebar/SidebarLink";

export default class TopbarProfile extends PureComponent {

    static propTypes = {
        onLogout: PropTypes.func.isRequired,
        user: PropTypes.object.isRequired
    };

    constructor() {
        super();
        this.state = {
            collapse: false,
        };
    }

    toggle = () => {
        this.setState(prevState => ({
          collapse: !prevState.collapse
        }));
    };

    render() {
        const { collapse } = this.state;
        const { onLogout, user } = this.props;

        return (
            <div className="topbar__profile">
                <button type="button" className="topbar__avatar" onClick={this.toggle}>
                    <DownIcon className="topbar__icon"/>
                </button>
                {collapse && <button type="button" className="topbar__back" onClick={this.toggle}/>}
                <Collapse isOpen={collapse} className="topbar__menu-wrap">
                    <div className="topbar__menu">
                        <TopbarMenuLink title="Корзина" icon="cart" path="/"/>
                        {/*  <div className="topbar__menu-divider"/>
                        <TopbarMenuLink title="Выход" clickMethod={onLogout} icon="exit" path="/log_in"/>*/
                        }
                    </div>
                </Collapse>
            </div>
        );
    }
}
