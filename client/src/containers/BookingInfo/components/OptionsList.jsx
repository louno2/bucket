import React from 'react';
import PropTypes from 'prop-types';

import {
    Table, Col, Card, CardBody
} from 'reactstrap';

import Alert from '../../../shared/components/Alert';

const OptionsList = ({dataList}) => {
    return (<Col md={12}>
            <Card>
                <CardBody>
                    <div className="card__title">
                        <h5 className="bold-text">Опции (options)</h5>
                    </div>
                    {dataList.length ? (<Table responsive className="table--bordered dashboard__table-pages">
                        <thead>
                        <tr>
                            <th>название:</th>
                            <th>ночей:</th>
                            <th>день с</th>
                            <th>день по</th>
                            <th>id продукта:</th>
                            <th>продукт:</th>
                        </tr>
                        </thead>
                        <tbody>
                            {dataList.map((data, index) => (
                                <tr key={index}>
                                    <td>{data.servicetypeoptionname}</td>
                                    <td>{data.packageoptionnumberofnights}</td>
                                    <td>{data.packageoptionfromdayseq}</td>
                                    <td>{data.packageoptiontodayseq}</td>
                                    <td>{data.serviceId}</td>
                                    <td>{data.servicelongname}</td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>) : <p>Нет данных</p>}
                  </CardBody>
            </Card>
        </Col>
    );
};

OptionsList.propTypes = {
    dataList: PropTypes.array.isRequired
};

export default OptionsList;
