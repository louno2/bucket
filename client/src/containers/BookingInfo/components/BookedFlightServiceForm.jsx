import React from 'react';
import {Card, CardBody, Col} from "reactstrap";
import {Field, reduxForm} from 'redux-form';

const renderField = ({
                         input, type
                     }) => (
    <div className="form__form-group-input-wrap">
        <input {...input} type={type}/>
    </div>
);

const BookedFlightServiceForm = () => {
    return ( <React.Fragment>
            <form className="form form--horizontal" onSubmit={()=>{}}>
                <div className="form__form-group">
                    <span className="form__form-group-label">id</span>
                    <div className="form__form-group-field">
                        <Field
                            name="bookedflightserviceId"
                            component={renderField}
                            type="text"
                        />
                    </div>
                </div>
                <div className="form__form-group">
                    <span className="form__form-group-label">Общая стоимость</span>
                    <div className="form__form-group-field">
                        <Field
                            name="bookedflightservicetotalcostamount"
                            component={renderField}
                            type="text"
                        />
                    </div>
                </div>
                <div className="form__form-group">
                    <span className="form__form-group-label">Общая цена продажи</span>
                    <div className="form__form-group-field">
                        <Field
                            name="bookedflightservicetotalsellingamount"
                            component={renderField}
                            type="text"
                        />
                    </div>
                </div>
                <div className="form__form-group">
                    <span className="form__form-group-label">Имя поставщика</span>
                    <div className="form__form-group-field">
                        <Field
                            name="suppliername"
                            component={renderField}
                            type="text"
                        />
                    </div>
                </div>
                <div className="form__form-group">
                    <span className="form__form-group-label">Имя поставщика (кратко)</span>
                    <div className="form__form-group-field">
                        <Field
                            name="suppliershortname"
                            component={renderField}
                            type="text"
                        />
                    </div>
                </div>
                <div className="form__form-group">
                    <span className="form__form-group-label">Статус поставщика</span>
                    <div className="form__form-group-field">
                        <Field
                            name="supplierstatus"
                            component={renderField}
                            type="text"
                        />
                    </div>
                </div>
                <div className="form__form-group">
                    <span className="form__form-group-label">Передача</span>
                    <div className="form__form-group-field">
                        <Field
                            name="transmissiontypename"
                            component={renderField}
                            type="text"
                        />
                    </div>
                </div>
                <div className="form__form-group">
                    <span className="form__form-group-label">Язык поставщика</span>
                    <div className="form__form-group-field">
                        <Field
                            name="languagename"
                            component={renderField}
                            type="text"
                        />
                    </div>
                </div>
            </form>
        </React.Fragment>
    )
};

export default reduxForm({
    form: 'booked_flight_service_form'
})(BookedFlightServiceForm);
