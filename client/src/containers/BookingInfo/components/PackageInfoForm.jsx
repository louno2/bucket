import {Card, CardBody, Col} from "reactstrap";
import {Field, reduxForm} from 'redux-form';
import React from "react";

const renderField = ({
  input, type
}) => (
    <div className="form__form-group-input-wrap">
        <input {...input} type={type}/>
    </div>
);

const PackageInfoForm = () => {
    return (<Col md={6}>
        <Card>
            <CardBody>
                <div className="card__title">
                    <h5 className="bold-text">Инф. о пакете</h5>
                </div>
                <form className="form form--horizontal" onSubmit={()=>{}}>
                    <div className="form__form-group">
                        <span className="form__form-group-label">id пакета</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packageId"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>
                    <div className="form__form-group">
                        <span className="form__form-group-label">Название</span>
                        <div className="form__form-group-field">
                            <Field
                                name="bookingname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>
                   <div className="form__form-group">
                        <span className="form__form-group-label">Описание</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packagelongname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>
                    <div className="form__form-group">
                        <span className="form__form-group-label">Название пакета (кратко)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packageshortname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Дата создания</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packagecreationdate"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Дата изменения</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packagemodificationdate"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">package min pax level</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packageminpaxlevel"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Тип пакета</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packagetypename"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Код типа пакета</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packagetypeshortcode"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Статус пакета</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packagestatusname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Статус (можно бронировать?)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packagestatuscanbook"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Статус (можно удалять)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packagestatuscandelete"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Статус (поиск бронирования)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packagestatusbookingsearch"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Статус (по умолчанию)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packagestatusisdefault"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">поиск статуса обслуживания пакета</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packagestatusmaintenancesearch"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Название пакета (кратко)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packageshortname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Организация</span>
                        <div className="form__form-group-field">
                            <Field
                                name="organisationname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Организация (тип)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="organisationtypename"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Организация (родитель)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="parent_organisationname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Валюта</span>
                        <div className="form__form-group-field">
                            <Field
                                name="currencyisocode"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Регион</span>
                        <div className="form__form-group-field">
                            <Field
                                name="regionname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>


                    <div className="form__form-group">
                        <span className="form__form-group-label">Регион (кратко)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="regionshortname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Анализ типа продаж 1 (название)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="salesanalysistype1name"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Анализ типа продаж 1 (по умолчанию)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="salesanalysistype1default"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Анализ типа продаж 1 (кратко)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="salesanalysistype1shortname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Анализ типа продаж 2 (название)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="salesanalysistype2name"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Анализ типа продаж 2 (по умолчанию)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="salesanalysistype2default"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Анализ типа продаж 2 (кратко)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="salesanalysistype2shortname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Анализ типа продаж 3 (название)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="salesanalysistype3name"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Анализ типа продаж 3 (по умолчанию)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="salesanalysistype3default"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Анализ типа продаж 3 (кратко)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="salesanalysistype3shortname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Кто создал</span>
                        <div className="form__form-group-field">
                            <Field
                                name="createdby_loginaccountname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Кто модифицировал</span>
                        <div className="form__form-group-field">
                            <Field
                                name="modifiedby_loginaccountname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Код продукта</span>
                        <div className="form__form-group-field">
                            <Field
                                name="productcodecode"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Описание продукта</span>
                        <div className="form__form-group-field">
                            <Field
                                name="productcodedescription"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Сообщение об отмене пакета</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packagecancellationmessage"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Сообщение об отмене пакета</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packagecancellationmessage"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Сообщение об отмене пакета</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packagecancellationmessage"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Название префикса бронирования</span>
                        <div className="form__form-group-field">
                            <Field
                                name="bookingprefixname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Описание префикса бронирования</span>
                        <div className="form__form-group-field">
                            <Field
                                name="bookingprefixdescription"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Рекомендуемый продукт</span>
                        <div className="form__form-group-field">
                            <Field
                                name="isrecommendedproduct"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">код группы по умолчанию для всех отправлений</span>
                        <div className="form__form-group-field">
                            <Field
                                name="defaultgroupcodeforalldepartures"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Расширение пакета</span>
                        <div className="form__form-group-field">
                            <Field
                                name="extensionpkg"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">isgthreshold</span>
                        <div className="form__form-group-field">
                            <Field
                                name="isgthreshold"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">условия пакета включений</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packagetermsinclusions"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">пакетное время обслуживания</span>
                        <div className="form__form-group-field">
                            <Field
                                name="packagetimeofservice"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                </form>
            </CardBody>
        </Card>
    </Col>)
};

export default reduxForm({
  form: 'package_info_form'
})(PackageInfoForm);
