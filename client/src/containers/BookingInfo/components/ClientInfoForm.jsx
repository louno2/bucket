import React from 'react';
import {Card, CardBody, Col} from "reactstrap";
import {Field, reduxForm} from 'redux-form';

const renderField = ({
                         input, type
                     }) => (
    <div className="form__form-group-input-wrap">
        <input {...input} type={type}/>
    </div>
);

const ClientInfoForm = () => {
    return ( <React.Fragment>
                <form className="form form--horizontal" onSubmit={()=>{}}>
                    <div className="form__form-group">
                        <span className="form__form-group-label">id</span>
                        <div className="form__form-group-field">
                            <Field
                                name="clientId"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Имя</span>
                        <div className="form__form-group-field">
                            <Field
                                name="clientname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Имя (кратко)</span>
                        <div className="form__form-group-field">
                            <Field
                                name="clientshortname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Статус</span>
                        <div className="form__form-group-field">
                            <Field
                                name="clientstatusname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Организация</span>
                        <div className="form__form-group-field">
                            <Field
                                name="organisationname"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Код организации</span>
                        <div className="form__form-group-field">
                            <Field
                                name="organisationcode"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Передача</span>
                        <div className="form__form-group-field">
                            <Field
                                name="transmissiontypename"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Язык</span>
                        <div className="form__form-group-field">
                            <Field
                                name="languagename"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Код языка</span>
                        <div className="form__form-group-field">
                            <Field
                                name="languagecode"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Комиссия утверждена</span>
                        <div className="form__form-group-field">
                            <Field
                                name="iscommisiontobeapproved"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Создан</span>
                        <div className="form__form-group-field">
                            <Field
                                name="clientcreationdate"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Обновлен</span>
                        <div className="form__form-group-field">
                            <Field
                                name="lastupdationdate"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Создал</span>
                        <div className="form__form-group-field">
                            <Field
                                name="createdby"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>

                    <div className="form__form-group">
                        <span className="form__form-group-label">Обновлен</span>
                        <div className="form__form-group-field">
                            <Field
                                name="modifiedby"
                                component={renderField}
                                type="text"
                            />
                        </div>
                    </div>
                </form>
        </React.Fragment>
            )
};

export default reduxForm({
    form: 'client_info_form'
})(ClientInfoForm);
