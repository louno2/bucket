import React from 'react';
import PropTypes from 'prop-types';

import {
    Table, Col, Card, CardBody
} from 'reactstrap';

const ServicesList = ({dataList}) => {
    return (<Col md={12}>
              <Card>
                <CardBody>
                    <div className="card__title">
                        <h5 className="bold-text">Продукты (services)</h5>
                    </div>

                    {dataList.length ? (<Table responsive className="table--bordered dashboard__table-pages">
                        <thead>
                        <tr>
                            <th>id:</th>
                            <th>название:</th>
                            <th>стоимость:</th>
                            <th>тип:</th>
                            <th>поставщик:</th>
                            <th>статус:</th>
                            <th>гео:</th>
                        </tr>
                        </thead>
                        <tbody>
                        {dataList.map((data, index) => (
                            <tr key={index}>
                                <td>{data.serviceId}</td>
                                <td>{data.servicelongname}</td>
                                <td>{data.currencyisocode} {data.bookedservicetotalcostamount}</td>
                                <td>{data.servicetypename}</td>
                                <td>{data.suppliername}</td>
                                <td>{data.servicestatusname}</td>
                                <td>{data.regionname}</td>
                            </tr>
                        ))}
                        </tbody>
                    </Table>) : <p>Нет данных</p>}
                </CardBody>
            </Card>
        </Col>
    );
};

ServicesList.propTypes = {
    dataList: PropTypes.array.isRequired
};

export default ServicesList;
