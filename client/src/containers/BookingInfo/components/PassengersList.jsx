import React from 'react';
import PropTypes from 'prop-types';

import {
    Table, Col, Card, CardBody
} from 'reactstrap';

const PassengersList = ({dataList}) => {
    return (<Col md={6}>
            <Card>
                <CardBody>
                    <div className="card__title">
                        <h5 className="bold-text">Пассажиры</h5>
                    </div>
                    {dataList.length ? (<Table responsive className="table--bordered dashboard__table-pages">
                        <thead>
                        <tr>
                            <th>id:</th>
                            <th>Имя:</th>
                            <th>Фамилия:</th>
                        </tr>
                        </thead>
                        <tbody>
                        {dataList.map((data, index) => (
                            <tr key={index}>
                                <td>{data.passengerId}</td>
                                <td>{data.passengerfirstname}</td>
                                <td>{data.passengerlastname}</td>
                            </tr>
                        ))}
                        </tbody>
                    </Table>) : <p>Нет данных</p>}
                </CardBody>
            </Card>
        </Col>
    );
};

PassengersList.propTypes = {
    dataList: PropTypes.array.isRequired
};

export default PassengersList;
