import React, {Component} from 'react';
import {compose} from 'redux';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import pick from 'lodash/pick';
import get from 'lodash/get';

import {
    Col, Container, Row,
    Card, CardBody
} from 'reactstrap';

import PackageInfoForm from './components/PackageInfoForm';
import ClientInfoForm from './components/ClientInfoForm';
import ServicesList from './components/ServicesList';
import OptionsList from './components/OptionsList';
import PassengersList from './components/PassengersList';
import BookedFlightServiceForm from './components/BookedFlightServiceForm';

import { loadBookingInfo, clearData } from '../../redux/actions/bookingActions';
import { withLoadingScreen } from '../Common/LoadingScreenManager';

const withHandleEmptyData = (WrappedComponent, data, headTitle, colWidth) => {
    return   <Col md={colWidth}>
        <Card>
            <CardBody>
                <div className="card__title">
                    <h5 className="bold-text">{headTitle}</h5>
                </div>
                {!isEmpty(data) ? (<WrappedComponent
                        onSubmit={() => {
                        }}
                        initialValues={data}/>) :
                    <p>Нет данных.</p>}

            </CardBody>
        </Card>
    </Col>
};

class BookingInfoWrap extends Component {

    static propTypes = {
        data: PropTypes.object.isRequired,
        loadBookingInfo: PropTypes.func.isRequired,
        clearData: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.loaderToggle = get(
          props.loadingScreen, 'toggle'
        );
    }

    componentDidMount() {

        const {
            match: {params},
            loadBookingInfo,
        } = this.props;

        loadBookingInfo(
            this.loaderToggle,
            params.id
        );
    };

    componentWillUnmount() {
       // this.props.clearData();
    };

    render() {

        const { data } = this.props;

        return (!isEmpty(data) &&
            (<Container className="dashboard">
                <Row>
                    <Col md={12}>
                        <h3 className="page-title">Информация о бронировании</h3>
                    </Col>
                </Row>
                <Row>
                    <PackageInfoForm
                        onSubmit={() => {
                        }}
                        initialValues={Object.assign(
                            pick(data.bookedPackage, ['bookingname']),
                            data.packageData
                        )}/>
                    {withHandleEmptyData(ClientInfoForm, data.client, 'Инф. о клиенте', 6)}
                </Row>
                <Row>
                    <ServicesList dataList={data.services}/>
                </Row>
                <Row>
                    <OptionsList dataList={data.options}/>
                </Row>
                <Row>
                    <PassengersList dataList={data.passengers}/>
                    {withHandleEmptyData(
                        BookedFlightServiceForm,
                        data.flightService,
                        'Инф. о перелете', 6)}
                </Row>
            </Container>));
    }
};

const mapStateToProps = ({booking}) => {
    return {
        data: booking.data
    }
};

export default compose(
    connect(mapStateToProps, {
        loadBookingInfo,
        clearData
    }),
    withLoadingScreen
)(BookingInfoWrap)
