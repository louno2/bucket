import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {compose} from 'redux';
import {withRouter} from 'react-router-dom';
import get from 'lodash/get';
import moment from 'moment';

import {
    Col, Container, Row,
    Button, ButtonToolbar
} from 'reactstrap';

import BookingList from './components/BookingList';
import Filters from './components/Filters';

import {loadBookingListData} from '../../redux/actions/bookingActions';
import {withLoadingScreen} from '../Common/LoadingScreenManager';

class BookingListWrap extends PureComponent {

    static propTypes = {
        dataList: PropTypes.array.isRequired,
        loadBookingListData: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
           initialValues: {
               intervalDate: {
                   startDate: moment()
                       .subtract(60, 'd')
                       .toDate(),
                   endDate: moment().toDate()
               }
           }
        };
        const {
            loadBookingListData,
            loadingScreen
        } = this.props;
        this.loadBookingListData = loadBookingListData
            .bind(this, loadingScreen.toggle);
    }

    componentDidMount() {
        this.applyFilters(
          get(this.state, 'initialValues')
        );
    };

    applyFilters = ({ intervalDate }) => {
        let params = { ...intervalDate };
        this.loadBookingListData(params);
    };

    render() {

        const {dataList} = this.props;
        const { initialValues } = this.state;

        return (
            <Container className="dashboard">
                <Row>
                    <Col md={12}>
                        <h3 className="page-title">Бронирования</h3>
                    </Col>
                </Row>
                <Row>
                   <Filters
                       initialValues={initialValues}
                       onSubmit={this.applyFilters}/>
                </Row>
                <Row>
                    <BookingList dataList={dataList}/>
                </Row>
            </Container>
        );
    };
}

const mapStateToProps = state => ({
    dataList: state.booking.dataList
});

export default compose(
    connect(mapStateToProps, {
        loadBookingListData
    }),
    withRouter,
    withLoadingScreen
)(BookingListWrap);
