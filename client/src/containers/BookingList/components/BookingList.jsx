import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import moment from 'moment';
import truncateString from '../../../utils/truncateString';

import {
  DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown, Table,
} from 'reactstrap';
import Panel from '../../../shared/components/Panel';
import DotsHorizontalIcon from 'mdi-react/DotsHorizontalIcon';

const DropDownMore = ({ bookingId }) => (
  <UncontrolledDropdown className="dashboard__table-more">
    <DropdownToggle>
      <p><DotsHorizontalIcon /></p>
    </DropdownToggle>
    <DropdownMenu className="dropdown__menu">
      <Link to={`/bookings/info/${bookingId}`}><DropdownItem>Общ. инф.</DropdownItem></Link>
      <Link to={`/bucket/prev/${bookingId}`}><DropdownItem>Корзина</DropdownItem></Link>
    </DropdownMenu>
  </UncontrolledDropdown>
);

DropDownMore.propTypes = {
  bookingId: PropTypes.number.isRequired
};

const BookingList = ({ dataList }) => {
  return (<Panel lg={12} title="Список">
    <Table responsive className="table--bordered dashboard__table-pages">
      <thead>
      <tr>
        <th></th>
        <th>id</th>
        <th>номер бронирования:</th>
        <th>название:</th>
        <th>дата бронирования:</th>
        <th>дата начала:</th>
        <th>дата окончания:</th>
        <th>ночей:</th>
        <th>Гео:</th>
        <th>Клиент:</th>
        <th>тип бронирования:</th>
        <th>статус бронирования</th>
        <th>login created</th>
        <th>login modified</th>
        <th>login secondary</th>
        <th>sales type1</th>
        <th>sales type2</th>
        <th>политика отмены</th>
        <th>тип отмены страховки</th>
      </tr>
      </thead>
      <tbody>
      {dataList.map((data, index) => (
        <tr key={index}>
          <td>
            <DropDownMore bookingId={data.bookingId} />
          </td>
          <td>{data.bookingId}</td>
          <td>{data.bookingreferencenumber}</td>
          <td>{truncateString(data.bookingname, 32)}</td>
          <td>{data.bookingbookingdate}</td>
          <td>{data.bookingstartdate}</td>
          <td>{data.bookingenddate}</td>
          <td>{data.bookingnumberofnights}</td>
          <td>{data.regionname}</td>
          <td>{data.clientname}</td>
          <td>{data.bookingtype}</td>
          <td>{data.bookingstatusname}</td>
          <td>{data.login_created}</td>
          <td>{data.login_modified}</td>
          <td>{data.login_secondary}</td>
          <td>{data.salesanalysistype1name}</td>
          <td>{data.salesanalysistype2name}</td>
          <td>{data.cancellationpolicyname}</td>
          <td>{data.cancellationinsurancetypename}</td>
        </tr>
      ))}
      </tbody>
    </Table>
  </Panel>)
};

BookingList.propTypes = {
  dataList: PropTypes.array.isRequired
};

export default BookingList;
