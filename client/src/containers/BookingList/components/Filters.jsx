import React from 'react';
import {
    Card, CardBody, Col,
    ButtonToolbar, Button
} from 'reactstrap';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import renderIntervalDatePickerField from '../../../shared/components/form/IntervalDatePicker';

const Index = ({ handleSubmit, initialValues }) => {
    return (<Col>
        <Card>
            <CardBody>
                <div className="card__title">
                    <h5 className="bold-text">Фильтр</h5>
                </div>
                <form className="form form--justify" onSubmit={handleSubmit}>
                    <div className="form__form-group">
                        <span className="form__form-group-label">Диапазон заезда</span>
                        <div className="form__form-group-field">
                            <Field
                                name="intervalDate"
                                component={renderIntervalDatePickerField}
                            />
                        </div>
                    </div>
                    <ButtonToolbar className="form__button-toolbar">
                        <Button color="primary" size="sm" type="submit">Применить</Button>
                    </ButtonToolbar>
                </form>
            </CardBody>
        </Card>
    </Col>);
};

Index.propTypes = {
    handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
    form: 'filter_form'
})(Index);
