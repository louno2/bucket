import React, {useState, useEffect, } from 'react';
import {  useSelector, useDispatch} from 'react-redux';
import {
    Card, CardBody, Col,
    Container, Row
} from 'reactstrap';

import PassengerList from './components/PassengerList';
import SeatingList from './components/SeatingList';

export default function Seating() {

    const dispatch = useDispatch();

    const { seating } = useSelector(
        state => state,
        () => {}
    );

    return (
        <Container className="dashboard">
            <Row>
                <Col md={12}>
                    <h3 className="page-title">Рассадка</h3>
                </Col>
            </Row>
            <Row>
                <PassengerList {...seating}/>
                <SeatingList seatings={seating.seatings} />
            </Row>
        </Container>
    )
}


