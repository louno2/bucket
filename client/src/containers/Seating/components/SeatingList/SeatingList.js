import React from 'react';
import {
    Card, CardBody, Col, Table
} from 'reactstrap';
import Alert from '../../../../shared/components/Alert';


export default function SeatingList({ seatings }) {
    return (
        <Col md={12} lg={12} xl={6}>
            <Card>
                <CardBody className="seating-list-component">
                    <div className="card__title">
                        <h5 className="bold-text">Рассадки</h5>
                    </div>
                    {seatings.length ?
                        <Table striped responsive style={{'marginBottom': '10px'}}>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Пассажиры</th>
                            </tr>
                            </thead>
                            <tbody>
                            {seatings.map(({title, passengInfo}, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{title}</td>
                                        <td>{passengInfo}</td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </Table>
                        : <Alert color="info" className="alert--bordered" icon>
                            <p>Начните добавлять рассадки для их вывода.</p>
                        </Alert>
                    }
                </CardBody>
            </Card>
        </Col>
    );
}
