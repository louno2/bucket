import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import {
    Card, CardBody, Col, Badge,
    Table, ButtonToolbar, Button,
} from 'reactstrap';

import CheckBox from '../../../../shared/components/form/CheckBox';
import showNotification from '../../../Common/Notification';
import {
    checkPassenger as attemptCheckPassenger,
    createSeating as attemptCreateSeating
} from '../../../../redux/actions/seatingActions';

export default function PassengerList({ seatings, passengers }) {

    const dispatch = useDispatch();

    const checkPassenger = (index) => {
        dispatch(attemptCheckPassenger(index));
    };

    const passengersOnSeating = passengers
        .filter(({ isCheck }) => isCheck);

    const createSeating = () => {
        dispatch(attemptCreateSeating(
           [ seatings, passengersOnSeating ],
           showNotification
        ));
    };

    return (
        <Col md={12} lg={12} xl={6}>
            <Card>
                <CardBody className="seating-list-component">
                    <div className="card__title">
                        <h5 className="bold-text">Список пассажиров</h5>
                    </div>
                    <Table striped responsive style={{'marginBottom': '10px'}}>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Имя, Фамилия</th>
                            <th>Дата рождения</th>
                            <th>Взр./Реб.</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {passengers.map(({name, date, isCheck}, index) => {
                            date = moment(date, 'DD.MM.YYYY');
                            let age = moment().diff(date, 'years');
                            return (
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td>{name}</td>
                                    <td>{date.format('DD.MM.YYYY')}</td>
                                    <td>
                                        {(() => {
                                            switch (true) {
                                                case age >= 18:
                                                    return <Badge color="success">Взрослый</Badge>
                                                case age < 18:
                                                    return <Badge color="primary">Ребенок</Badge>
                                            }
                                        })()}
                                    </td>
                                    <td>
                                        <CheckBox
                                            onChange={checkPassenger.bind(null, index)}
                                            name={`checkbox-${index}`}
                                            className="colored-click"
                                            defaultChecked={isCheck}
                                            value={isCheck}
                                        />
                                    </td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </Table>
                    <ButtonToolbar style={{'float': 'right'}}>
                        <Button onClick={createSeating}
                                disabled={!passengersOnSeating.length}
                                size="sm"
                                color="primary">
                            Создать рассадку
                        </Button>
                    </ButtonToolbar>
                </CardBody>
            </Card>
        </Col>);
}
