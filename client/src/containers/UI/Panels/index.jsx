import React from 'react';
import { Col, Container, Row } from 'reactstrap';
import PropTypes from 'prop-types';
import DefaultPanel from './components/DefaultPanel';

const Panels = () => (
  <Container>
    <Row>
      <Col md={12}>
        <h3 className="page-title">Example here</h3>
        <h3 className="page-subhead subhead">Use this elements, if you want to show some hints
              or additional information
        </h3>
      </Col>
    </Row>
    <Row>
      <DefaultPanel />
    </Row>
  </Container>
);


export default Panels;
