import React, {Component, Fragment} from 'react';
import {BrowserRouter} from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import {Provider} from 'react-redux';
import '../../scss/bootstrap.css';
import '../../scss/app.scss';

import store from './store';
import Router from './Router';
import ScrollToTop from './ScrollToTop';
import setAuthToken from '../../utils/setAuthToken';
import {setCurrentUser, logoutUser} from '../../redux/actions/authActions';

import {LoadingProvider} from '../Common/LoadingScreenManager'

// Check for token
if (localStorage.jwtToken) {

    // Set auth token header auth
    setAuthToken(localStorage.jwtToken);
    // Decode token and get user info and exp
    const decoded = jwt_decode(localStorage.jwtToken);
    // Set user and isAuthenticated
    store.dispatch(setCurrentUser(decoded));

    // Check for expired token
    const currentTime = Date.now() / 1000;
    if (decoded.exp < currentTime) {
        // Logout user
        store.dispatch(logoutUser());
        // Clear current Profile
    }
}

class App extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <Provider store={store}>
                <BrowserRouter>
                    <LoadingProvider>
                        <ScrollToTop>
                                <Router/>
                        </ScrollToTop>
                    </LoadingProvider>
                </BrowserRouter>
            </Provider>
        )
    }
}

export default App;
