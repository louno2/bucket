import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Layout from '../Layout/index';
import MainWrapper from './MainWrapper';
import Bucket from '../Bucket/index';
import Seating from '../Seating';

const Router = () => (
    <MainWrapper>
        <main>
            <Layout/>
            <div className="container__wrap">
                <Switch>
                    <Route path="/seating" component={Seating}/>
                    <Route path="/" component={Bucket}/>
                </Switch>
            </div>
        </main>
    </MainWrapper>
);

export default Router;
