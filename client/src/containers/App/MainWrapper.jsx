import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import isNull from 'lodash/isNull';
import isNaN from 'lodash/isNaN';
import { ThemeProps } from '../../shared/prop-types/ReducerProps';

import { setCrmParams } from '../../redux/actions/crmParamsActions';
import { getPresetData } from '../../redux/actions/bucketActions';
import {
  getPassengers,
  getSeatings
} from '../../redux/actions/seatingActions';

class MainWrapper extends PureComponent {

  static propTypes = {
    theme: ThemeProps.isRequired,
    children: PropTypes.element.isRequired,
  };

  componentDidMount() {

    let searchParams = new URLSearchParams(
        window.location.search
    );
    const [
       leadId, orderId
    ] = [ 'id', 'order' ].map(name => {
       let value = parseInt(searchParams.get(name));
       return !isNaN(value) ? value : null;
    });

    const {
      setCrmParams,
      getPresetData,
      getPassengers,
      getSeatings
    } = this.props;

    this.props.setCrmParams({
      leadId, orderId
    });

    let params;
    switch (true) {
      case !isNull(orderId):
        params = {
          id: orderId,
          urlPart: 'getorder'
        };
        break;
      case !isNull(leadId):
        params = {
          id: leadId,
          urlPart: 'lead'
        };
        break;
      default:
        params = {};
    };

    getPresetData(params, this.loaderToggle);

    !isNull(leadId) && getPassengers(leadId);
    !isNull(orderId) && getSeatings(orderId)

  }

  render() {

    const {
      theme, children,
      crmParams = {}
    } = this.props;

    return (
      <div className={theme.className}>
        <div className="wrapper ltr-support">
          {!isNull(crmParams.leadId) && children}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({theme, crmParams}) => ({
  theme: theme,
  crmParams
});

export default connect(
    mapStateToProps, {
      setCrmParams,
      getPresetData,
      getPassengers,
      getSeatings
})(MainWrapper);
