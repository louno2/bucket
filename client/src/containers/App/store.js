
import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { reducer as reduxFormReducer } from 'redux-form';
import {
  sidebarReducer,
  themeReducer,
  authReducer,
  bookingReducer,
  rtlReducer,
  bucketGridPrevReducer,
  bucketReducer,
  seatingReducer,
  crmParamsReducer
} from '../../redux/reducers/index';

const reducer = combineReducers({
  form: reduxFormReducer, // mounted under "form",
  theme: themeReducer,
  sidebar: sidebarReducer,
  auth: authReducer,
  booking: bookingReducer,
  rtl: rtlReducer,
  bucketGridPrev: bucketGridPrevReducer,
  bucket: bucketReducer,
  seating: seatingReducer,
  crmParams: crmParamsReducer
});

const initialState = {};
const middleware = [thunk];

const store = createStore(
    reducer,
    initialState,
    compose(
        applyMiddleware(...middleware)
    )
);

export default store;
