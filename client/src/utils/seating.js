import moment from 'moment';

function seatingFormat(adult, children) {

    let childAgeSets = children.length > 0 ? children.join(',') : null;
    let childLabel = childAgeSets ? `/ детск. (${childAgeSets})` : '';
    let occupId = `adult-${adult}:child-${childAgeSets || 0}`;
    let label = `взр. ${adult} ${childLabel}`;

    return { occupId, label }
}

function prepareSeating([ seatings, passengers ]) {

    let occupValue = passengers
        .map(({ date }) => date)
        .reduce((store, date) => {
                let age = moment().diff(
                    moment(date, 'DD.MM.YYYY'), 'years'
                );
                if (age < 18) store.children.push(age);
                else store.adult++;
                return store;
            }, { adult: 0, children: [] }
        );

    let { adult, children } = occupValue;
    let {
        occupId, label
    } = seatingFormat(
        adult, children
    );

    let numOfExists = seatings.filter(
      ({ occupInfo : { id } }) => id === occupId
    );

    return {
        id: seatings.length + 1,
        title: `${numOfExists.length > 0 ? `(${numOfExists.length + 1})` : ''} ${label}`,
        passengInfo: passengers.map(({ name }) => name).join(', '),
        occupInfo: {
            id: occupId,
            value: occupValue
        }
    }
}

export {
   prepareSeating,
   seatingFormat
}
