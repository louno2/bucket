import result from 'lodash/result';
import isString from 'lodash/isString';

let mapMessages = {
  CREATE: 'Произошла ошибка при создании',
  GETTING: 'Произошла ошибка при получении объектов',
  REMOVE: 'Произошла ошибка при удалении'
};

export default (action, callback, err) => {

  console.error(err);
  let [ errorsObj, errorMessage ] = ['errors', 'message']
      .map(key => result(err, `response.data.${key}`, {}));

  if (isString(errorMessage)) {
    Object.assign(errorsObj, {errorStd: errorMessage});
  }

  for (let errorKey in  errorsObj) {

    let notifyObj ={
      message: errorsObj[errorKey] || mapMessages[action],
      type: 'danger'
    };

    switch (callback.name) {
      case 'showNotification': callback(notifyObj); break;
      case '':
      case 'loadingScreen': {
        callback(false, notifyObj);
      } break;
    }
  }
};
