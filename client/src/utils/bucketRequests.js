import axios from 'axios';
import get from 'lodash/get';
import pick from 'lodash/pick';

const {API_URL = '' } = process.env;

let searchGeo, searchProducts, collectedProducts, getPrice;

if (/localhost/.test(API_URL)) {

    searchGeo = (params) => {
        let { term = '' } = params;
        return axios
         .get(`${API_URL}/api/remote/search-geo/?term=${term}`)
         .then(({ data: { geoList }}) => (
             Promise.resolve(geoList)
         ))
        .catch(console.error);
    };

    searchProducts = (data) => {
        return axios
            .post(`${API_URL}/api/remote/search-products`, data)
            .then(({ data: { productList }}) => (
                Promise.resolve(productList)
            ))
            .catch(console.error)
    };

    collectedProducts = (data) => {

        return axios
            .post(`${API_URL}/api/remote/collected-products`, data)
            .then(({data: {productList}}) => {
                return Promise.resolve(productList)
            })
            .catch(console.error)
    };

    getPrice = (params) => {
        return axios
            .get(`${API_URL}/api/remote/get-price`, { params })
            .then(({ data }) => {
                return Promise.resolve(pick(data, [
                        'priceDataSingle',
                        'priceDataByDays'
                    ])
                )})
            .catch(console.error)
    };
} else {

    searchGeo = (params) => {
        let {term = ''} = params;
        return axios
            .get(`/booking/geo?term=${term}`)
            .then(({data}) => (
                Promise.resolve(get(data, 'result', []))
            ))
            .catch(console.error);
    };


    searchProducts = (data) => {
        return axios
            .post('/booking/product/search', {params: {data}})
            .then(({data}) => (
                Promise.resolve(get(
                    data, 'result.productList', []
                ))
            ))
            .catch(console.error)
    };


    collectedProducts = (data) => {
        return axios
            .post('/booking/product/get', {params: {data}})
            .then(({data}) => (
                Promise.resolve(get(
                    data, 'result.productList', {}
                ))
            ))
            .catch(console.error)
    };


    getPrice = ({productId, optionId, sourceId, params: data}) => {

        return axios
            .post(`/booking/option/get/${productId}/${optionId}/${sourceId}`, {params: {data}})
            .then(({data}) => {
                return Promise.resolve(pick(data.result, [
                        'priceDataSingle',
                        'priceDataByDays'
                    ])
                )
            })
            .catch(console.error)
    };
}


export {
    searchGeo,
    searchProducts,
    collectedProducts,
    getPrice
};
