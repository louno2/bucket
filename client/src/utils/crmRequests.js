import axios from 'axios';
import moment from 'moment';
import isNull from 'lodash/isNull';
import get from 'lodash/get';
import cloneDeep from 'lodash/cloneDeep';
import handleErrors from './handleErrors';
const {API_URL = '' } = process.env;

const createCommercialOffer = (leadId, products, presetData, currency, loadingScreen) =>  {

    if (!/localhost/.test(API_URL) && !isNull(leadId)) {

        let copyPreset = cloneDeep(presetData);
        copyPreset.fromDate = moment(presetData.fromDate)
            .format('DD-MM-YYYY');

        let params = {
            data: {
              products,
              presetData: Object.assign(
               {}, currency, copyPreset
              )
            }
        };
        loadingScreen(true);
        return axios
            .post(`/booking/order/${leadId}`, { params })
            .then(({data}) => {

                const { status, ref : orderId } = get(data, 'result', {});
                let isSuccess = status === 'OK';

                loadingScreen(false, {
                    type: isSuccess ? 'success' : 'danger',
                    message: isSuccess
                        ? 'Запрос на создание коммерческого предложения выполнен успешно'
                        : 'Ошибка при выполнении запроса'
                });
                return Promise.resolve(orderId)
            })
            .catch(handleErrors.bind(
                this, 'GETTING', loadingScreen
            ))
    }
};

const saveSeatings = (orderId, data, loadingScreen) => {
    if (!/localhost/.test(API_URL)) {
        loadingScreen(true);
        axios
            .post(`/booking/seating/${orderId}`, {params: { data }})
            .then(({data}) => {
                const isSuccess = get(data, 'result.status', null) === 'OK';
                loadingScreen(false, {
                    type: isSuccess ? 'success' : 'danger',
                    message: isSuccess
                        ? 'Рассадки сохранены'
                        : 'Ошибка при выполнении запроса'
                });

            })
            .catch(handleErrors.bind(
                this, 'GETTING', loadingScreen
            ))
    }
};

export {
    createCommercialOffer,
    saveSeatings
}
