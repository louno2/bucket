import axios from 'axios';
import first from 'lodash/first';
import last from 'lodash/last';
import inRange from 'lodash/inRange';
import uniq from 'lodash/uniq';
import moment from 'moment';

function range(start, end, length = end - start + 1) {
    return Array.from({length}, (_, i) => start + i);
}

function getDaysNumber(period, startDate) {
    return period.map(date => moment(date)
        .diff(moment(startDate, 'DD-MM-YYYY'), 'days')
    );
}

function actionsOnProducts(list, startDate, duration) {

    return {
        collectPeriods() {

            return list.map(({fromDate, toDate}) => {
                let [fromDay, toDay] = getDaysNumber(
                    [fromDate, toDate], startDate
                );
                return range(fromDay, toDay);
            });
        },
        groupByPeriods(periods) {
            return list
                .reduce((store, current) => {

                    let {fromDate, toDate} = current;
                    let [fromDayNext, toDayNext] = getDaysNumber(
                        [fromDate, toDate], startDate
                    );
                    let matched = store.find(item => {
                        let [
                            fromDayPrev, toDayPrev
                        ] = item.period;
                        return [fromDayNext, toDayNext].every(
                            day => inRange(day,
                                fromDayPrev,
                                toDayPrev + 1
                            ));
                    });
                    if (matched) {
                        matched.products.push(current);
                    }
                    return store;

                }, periods);
        },
        collectGridRows(productGroups) {

            const { dailyLayout } =  [...Array(duration).keys()]
                .reduce((store, dayNum) => {

                    let {
                        dailyLayout,
                        excludedDays
                    } = store;


                    if (excludedDays.includes(dayNum)) {
                        return store;
                    }

                    let matched = productGroups.find(({period}) => {
                        let [fromDay, toDay] = period;
                        return inRange(dayNum, fromDay, toDay + 1);
                    });

                    const addDayToLayout = (num) => dailyLayout.push({
                        date: moment(startDate, 'DD-MM-YYYY')
                            .add(num, 'days')
                            .format('DD.MM.YYYY'),
                        isEmpty: true
                    });

                    if (matched) {

                        dailyLayout.push(...matched.products
                            .sort((prev, next) => {
                                let [
                                    fromDayPrev,
                                    fromDayNext
                                ] = [prev, next]
                                    .map(({fromDate}) => moment(fromDate));

                                return fromDayPrev.diff(startDate, 'days') - fromDayNext.diff(startDate, 'days')
                            })
                            .map(product => {
                                let {fromDate: fd, toDate: td} = product;
                                let date = [fd, td].map(d =>
                                    moment(d).format('DD.MM')
                                ).join('-');
                                return Object.assign(product, {date});
                            }));


                        let [fromDay, toDay] = matched.period;
                        excludedDays = uniq(excludedDays
                            .push(...range(...matched.period))
                        );
                        addDayToLayout(toDay);
                    } else {
                        addDayToLayout(dayNum);
                    }
                    return store

                }, {dailyLayout: [], excludedDays: []});

            return dailyLayout;
        }
    }
}

function mergeIntersections(arrays) {
    const pools = {};
    arrays.map(arr => new Set(arr)).forEach(set => {
        Array.from(set).forEach(item => {
            if (!pools[item]) {
                pools[item] = set;
            } else {
                pools[item].forEach(item2 => {
                    pools[item2] = set.add(item2);
                });
            }
        });
    });

    const seen = {};
    const merged = [];
    Object.keys(pools).forEach(item => {
        if (!seen[item]) {
            merged.push(Array.from(pools[item]).sort((a, b) => a - b));
            pools[item].forEach(item2 => seen[item2] = 1);
        }
    });

    return merged.map(array => [first(array), last(array)])
        .sort(((prev, next) => {
            let fromDayPrev = first(prev);
            let fromDayNext = first(next);
            return fromDayPrev - fromDayNext;
        }))
        .map(period => ({period, products: []}))
};

function preparePresetData(data) {

    const {
        fromDate, duration
    } = data;

    return Object.assign(data, {
        fromDate: moment(fromDate, 'DD-MM-YYYY')
            .toDate(),
        toDate: moment(fromDate, 'DD-MM-YYYY')
            .add(duration, 'days')
            .toDate()
    });
};

export {
    actionsOnProducts,
    mergeIntersections,
    preparePresetData,
    range
}
