import cloneDeep from 'lodash/cloneDeep';
import {
    SET_CRM_PARAMS
} from '../actions/crmParamsActions';

const initialState = {
    leadId: null,
    orderId: null
};

//orderId - на получечение предустановок, продуктов, рассадок.

export default function (state = initialState, action) {

    switch (action.type) {
        case SET_CRM_PARAMS: {
            let params = cloneDeep(action.payload);
            return Object.assign({},
              state, params
            )
        }
        default:
            return state;
    }
}
