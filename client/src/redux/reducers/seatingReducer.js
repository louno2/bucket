import update from 'immutability-helper';

import {
    SET_PASSENGERS,
    CHECK_PASSENGER,
    SET_SEATINGS, CREATE_SEATING
} from '../actions/seatingActions';

const initialState = {
    passengers: [],
    seatings: []
};

export default function (state = initialState, action) {

    switch (action.type) {
        case SET_PASSENGERS: {
            return update(state, {
                passengers: {
                    $set: action.passengers.map(passenger =>
                        Object.assign(passenger, {isCheck: false})
                    )
                }
            });
        }
        case SET_SEATINGS: {
            return update(state, {
                seatings: {
                    $set: action.seatings
                }
            });
        }
        case CHECK_PASSENGER: {

            const index = action.index;
            let current = state.passengers[index];
            const modifiedPassenger = update(current, {
                isCheck: {$apply: x => !x},
            });
            const updatedAtIndex = {
                $splice: [[
                    index, 1, modifiedPassenger
                ]]
            };
            return update(state, { passengers: updatedAtIndex });
        }
        case CREATE_SEATING: {
            return update(state, {
                seatings: {
                    $push: [action.seating]
                }
            })
        }
        default:
            return state
    }
}
