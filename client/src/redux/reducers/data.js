export default [{
    "hash": "cbd4803b-757a-41b8-994e-06deaeb06f68",
    "occupancy": {
        "adult": 2,
        "children": []
    },
    "product": {
        "id": "55842764fd77c61321285ac8",
        "title": "Scandic Copenhagen",
        "source": {
            "iid": "553d21d493eee6e7f89516df",
            "title": "Travel Studio"
        }
    },
    "type": "placement",
    "geo": {
        "id": 27,
        "title": "Дания"
    },
    "option": {
        "id": "55842764fd77c61321285ad3",
        "title": "DBL Standard"
    },
    "fromDate": "2020-04-14T21:00:00.000Z",
    "toDate": "2020-04-22T21:00:00.000Z",
    "duration": 11,
    "priceData": {
        "adult": 60758,
        "total": 121515,
        "currency": {
            "symbol": "Руб",
            "code": "RUB"
        },
        "itineraryId": null,
        "date": {
            "start": "2020-04-15T00:00:00+03:00",
            "end": "2020-04-23T00:00:00+03:00",
            "duration": 8
        },
        "buyPriceOriginal": {
            "total": 8000,
            "currency": "DKK"
        },
        "sellPriceOriginal": {
            "total": 1376,
            "currency": "EUR"
        },
        "buyPrice": {
            "total": 95360,
            "currency": "RUB"
        }
    },
    "date": "15.04-23.04"
},
    {
        "hash": "a27f745b-02a7-4b9e-9ced-4ca84299c0b1",
        "occupancy": {
            "adult": 2,
            "children": []
        },
        "product": {
            "id": "55841dedfd77c61321277232",
            "title": "Scandic Sluseholmen",
            "source": {
                "iid": "553d21d493eee6e7f89516df",
                "title": "Travel Studio"
            }
        },
        "type": "placement",
        "geo": {
            "id": 27,
            "title": "Дания"
        },
        "option": {
            "id": "55841dedfd77c61321277239",
            "title": "DBL Standard"
        },
        "fromDate": "2020-04-15T21:00:00.000Z",
        "toDate": "2020-04-17T21:00:00.000Z",
        "duration": 11,
        "priceData": {
            "adult": 12187,
            "total": 24374,
            "currency": {
                "symbol": "Руб",
                "code": "RUB"
            },
            "itineraryId": null,
            "date": {
                "start": "2020-04-16T00:00:00+03:00",
                "end": "2020-04-18T00:00:00+03:00",
                "duration": 2
            },
            "buyPriceOriginal": {
                "total": 1600,
                "currency": "DKK"
            },
            "sellPriceOriginal": {
                "total": 276,
                "currency": "EUR"
            },
            "buyPrice": {
                "total": 19072,
                "currency": "RUB"
            }
        },
        "date": "16.04-18.04"
    },
    {
        "hash": "5af70f55-f895-42fa-87d5-ca5b88102a22",
        "occupancy": {
            "adult": 2,
            "children": []
        },
        "product": {
            "id": "5584276ffd77c61321285d3d",
            "title": "Scandic Eremitage",
            "source": {
                "iid": "553d21d493eee6e7f89516df",
                "title": "Travel Studio"
            }
        },
        "type": "placement",
        "geo": {
            "id": 27,
            "title": "Дания"
        },
        "option": {
            "id": "5584276ffd77c61321285d4e",
            "title": "DBL Superior"
        },
        "fromDate": "2020-04-21T21:00:00.000Z",
        "toDate": "2020-04-23T21:00:00.000Z",
        "duration": 11,
        "priceData": {
            "adult": 16250,
            "total": 32499,
            "currency": {
                "symbol": "Руб",
                "code": "RUB"
            },
            "itineraryId": null,
            "date": {
                "start": "2020-04-22T00:00:00+03:00",
                "end": "2020-04-24T00:00:00+03:00",
                "duration": 2
            },
            "buyPriceOriginal": {
                "total": 2140,
                "currency": "DKK"
            },
            "sellPriceOriginal": {
                "total": 368,
                "currency": "EUR"
            },
            "buyPrice": {
                "total": 25509,
                "currency": "RUB"
            }
        },
        "date": "22.04-24.04"
    }
];
