import cloneDeep from 'lodash/cloneDeep';
import update from 'immutability-helper';
import {v4 as uuidv4} from 'uuid';
import {
    ADD_TO_GRID,
    SET_PRESET_DATA,
    REMOVE_FROM_GRID,
    SET_CRM_PARAMS,
    SET_PRODUCTS_DATA,
    GRID_ROWS_GENERATE
} from '../actions/bucketActions';

import {
    actionsOnProducts,
    mergeIntersections,
    range, preparePresetData
} from '../../utils/bucketGrids';

import sampleGridRows from './data';

const initialState = {
    products: [],
    gridRows: [],
    currency: {},
    presetData: {}
};

export default function (state = initialState, action) {

    switch (action.type) {
        case ADD_TO_GRID: {

            let onAdd = Object.assign(
                {hash: uuidv4()}, action.product
            );

            return update(state, {
                products: {
                    $push: [onAdd]
                }
            });
        }
        case SET_PRODUCTS_DATA: {

            return update(state, {
                products: {
                    $set: action.products
                }
            });
        }
        case GRID_ROWS_GENERATE: {

            const {
                presetData : {
                    duration,
                    fromDate: startDate
                },
                products
            } = state;


            const {
                collectPeriods,
                groupByPeriods,
                collectGridRows
            } = actionsOnProducts(
                products, startDate,
                duration
            );

            let allPeriods = collectPeriods();
            let sepPeriods = mergeIntersections(allPeriods);
            let productGroups = groupByPeriods(sepPeriods);

            return update(state, {
               gridRows : { $set: collectGridRows(productGroups) }
            });
        }
        case REMOVE_FROM_GRID: {

            let index = state.products
                .map(({hash}) => hash)
                .indexOf(action.hash);

            return update(state, {
                products: {$splice: [[index, 1]]}
            });
        }
        case SET_PRESET_DATA: {

            let {
                code, symbol, ...rest
            } = cloneDeep(action.payload);

            return {
                ...state,
                currency: {code, symbol},
                presetData: preparePresetData(rest),

            }
        }
        default:
            return state;
    }
}
