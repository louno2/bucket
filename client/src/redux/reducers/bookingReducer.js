import {
  LOAD_BOOKING_LIST_DATA,
  LOAD_BOOKING_DATA
} from '../actions/bookingActions';

const initialState = {
  dataList: [],
  data: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOAD_BOOKING_LIST_DATA: {
      return { ...state, dataList: action.payload };
    }
    case LOAD_BOOKING_DATA:
      return { ...state,
        data: Object.assign({}, action.payload)
      };
    default:
      return state;
  }
}
