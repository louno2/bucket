import themeReducer from './themeReducer';
import sidebarReducer from './sidebarReducer';
import authReducer from './authReducer';
import bookingReducer from './bookingReducer';
import rtlReducer from './rtlReducer';
import bucketGridPrevReducer from './bucketGridPrevReducer';
import bucketReducer from './bucketReducer';
import seatingReducer from './seatingReducer';
import crmParamsReducer from './crmParamsReducer';

export {
  themeReducer,
  sidebarReducer,
  authReducer,
  bookingReducer,
  rtlReducer,
  bucketReducer,
  seatingReducer,
  bucketGridPrevReducer,
  crmParamsReducer
};
