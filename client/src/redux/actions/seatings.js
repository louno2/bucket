export default [{
    "id": 1,
    "title": " \u0432\u0437\u0440. 3 ",
    "passengInfo": "Yaroslav Kosov, \u0410\u043d\u043d\u0430 \u041c\u0438\u043b\u044e\u0442\u0438\u043d\u0430, \u0418\u0440\u0438\u043d\u0430 \u0416\u0438\u0432\u043e\u0442\u044f\u0433\u0438\u043d\u0430",
    "occupInfo": {"id": "adult-3:child-0", "value": {"adult": 3, "children": []}}
}, {
    "id": 2,
    "title": " \u0432\u0437\u0440. 2 ",
    "passengInfo": "\u0410\u043d\u043d\u0430 \u041c\u0438\u043b\u044e\u0442\u0438\u043d\u0430, \u0418\u0440\u0438\u043d\u0430 \u0416\u0438\u0432\u043e\u0442\u044f\u0433\u0438\u043d\u0430",
    "occupInfo": {"id": "adult-2:child-0", "value": {"adult": 2, "children": []}}
}]
