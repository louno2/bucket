import axios from 'axios';
import handleErrors from '../../utils/handleErrors';

export const LOAD_BOOKING_LIST_DATA = 'LOAD_BOOKING_LIST_DATA';
export const LOAD_BOOKING_DATA = 'LOAD_BOOKING_DATA';
export const CLEAR_DATA = 'CLEAR_DATA';

const {API_URL = ''} = process.env;

export const loadBookingListData = (loadingScreen, params)  => (dispatch) => {
    loadingScreen(true);
    axios
        .post(`${API_URL}/api/remote/bookings`,
            Object.assign(params, { isModify: true})
        )
        .then(({data: { bookingData }}) => {
            loadingScreen(false);
            dispatch({
                type: LOAD_BOOKING_LIST_DATA,
                payload: bookingData
            })
        })
        .catch(handleErrors.bind(
            this, 'GETTING', loadingScreen
        ));
};

export const loadBookingInfo = (loadingScreen, id) => (dispatch) => {
  loadingScreen(true);
  axios
      .get(`${API_URL}/api/remote/bookings/${id}/?isModify=true`)
      .then(({data: { bookingData }}) => {
          loadingScreen(false);
          dispatch({
              type: LOAD_BOOKING_DATA,
              payload: bookingData
          })
      })
      .catch(handleErrors.bind(
          this, 'GETTING', loadingScreen
      ));
};

export const clearData = () => (dispatch) => {
    dispatch({
        type: LOAD_BOOKING_DATA,
        payload: {}
    })
};
