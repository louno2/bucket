import axios from 'axios';
import cloneDeep from 'lodash/cloneDeep';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import handleErrors from '../../utils/handleErrors';
const {API_URL = '' } = process.env;


import sampleGridRows from '../reducers/data';

export const ADD_TO_GRID = 'ADD_TO_GRID';
export const SET_PRESET_DATA = 'SET_PRESET_DATA';
export const SET_PRODUCTS_DATA = 'SET_PRODUCTS_DATA';
export const REMOVE_FROM_GRID = 'REMOVE_FROM_GRID';
export const GRID_ROWS_GENERATE = 'GRID_ROWS_GENERATE';

const presetSample = {
    "code": "RUB",
    "symbol": "&#8381;",
    "fromDate": "27-04-2020",
    "duration": 20,
    "geo": {
        "id": 27,
        "title": "\u0414\u0430\u043d\u0438\u044f"
    },
    "occupancy": {
        "adult": 2,
        "children": []
    }
};

export const addToGrid = (product) => (dispatch) => {
    dispatch({
        type: ADD_TO_GRID,
        product
    });
    dispatch({ type: GRID_ROWS_GENERATE });
};

export const removeFromGrid = (hash) => (dispatch) => {
    dispatch({
        type: REMOVE_FROM_GRID,
        hash
    });
    dispatch({ type: GRID_ROWS_GENERATE });
};

export const getPresetData = (params, loadingScreen) => (dispatch) => {

    if (!/localhost/.test(API_URL)) {
        let { id, urlPart } = params;
        axios
            .get(`/booking/${urlPart}/${id}`)
            .then(({data}) => {

                const {
                  presetData, products = []
                } = get(data, 'result', {});

                dispatch({
                    type: SET_PRESET_DATA,
                    payload: presetData
                });

                if (products.length) {
                    dispatch({
                        type: SET_PRODUCTS_DATA,
                        products
                    });
                }
                dispatch({
                    type: GRID_ROWS_GENERATE
                });
            })
            .catch(handleErrors.bind(
                this, 'GETTING', loadingScreen
            ))
    } else {
        dispatch({
            type: SET_PRESET_DATA,
            payload: presetSample
        });
        dispatch({
            type: SET_PRODUCTS_DATA,
            products: sampleGridRows
        });
        dispatch({ type: GRID_ROWS_GENERATE });
    }
};
