import axios from 'axios';
import get from 'lodash/get';
const {API_URL = '' } = process.env;

import { prepareSeating } from '../../utils/seating';

export const SET_PASSENGERS = 'SET_PASSENGERS';
export const CHECK_PASSENGER = 'CHECK_PASSENGER';
export const SET_SEATINGS = 'SET_SEATINGS';
export const CREATE_SEATING = 'CREATE_SEATING';

import passengersData from './passengers';
import seatings from './seatings';

export const getPassengers = (leadId) => (dispatch) => {

    if (!/localhost/.test(API_URL)) {
        axios
            .get(`/booking/passengers/${leadId}`)
            .then(({data}) => {
                dispatch({
                    type: SET_PASSENGERS,
                    passengers: get(data, 'result', [])
                });
            })
            .catch(console.error);
    } else {
        dispatch({
            type: SET_PASSENGERS,
            passengers: passengersData
        });
    }
};

export const getSeatings = (orderId) => (dispatch) => {

    if (!/localhost/.test(API_URL)) {
        axios
            .get(`/booking/getseating/${orderId}`)
            .then(({data}) => {
                dispatch({
                    type: SET_SEATINGS,
                    seatings: get(data, 'result', [])
                });
            })
            .catch(console.error);
    } else {
        dispatch({
            type: SET_SEATINGS,
            seatings: seatings
        });
    }
};

export const checkPassenger = (index) => (dispatch) => {
    dispatch({
        type: CHECK_PASSENGER,
        index
    })
};

export const createSeating = (data, showNotification) => (dispatch) => {
    dispatch({
        type: CREATE_SEATING,
        seating: prepareSeating(data)
    })
};
