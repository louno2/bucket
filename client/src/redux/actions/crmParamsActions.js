
export const SET_CRM_PARAMS = 'SET_CRM_PARAMS';

export const setCrmParams = (data) => (dispatch) => {
    dispatch({
        type: SET_CRM_PARAMS,
        payload: data
    });
};
